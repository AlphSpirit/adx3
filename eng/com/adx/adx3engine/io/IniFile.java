package com.adx.adx3engine.io;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * INI (.ini) buffered file parser.
 * The parser buffers every section/key/value couples in a HashMap. This way, the whole content of the file is read and the file is closed afterwards.
 * Changing values is very fast and when the user is done, everything is written back into the file.
 * @author Alexandre Desbiens
 */
public class IniFile {

	private HashMap<String, HashMap<String, String>> map = new LinkedHashMap<String, HashMap<String, String>>();
	private String path;
	private boolean readOnly = false;
	private TextFile file;

	/**
	 * Initializes the parser with the path to the file to read.
	 * @param path Path to source file
	 */
	public IniFile(String path) {
		this(path, false);
	}

	/**
	 * Initializes the parser with the path to the file to read and a read-only flag.
	 * @param path Path to source file
	 * @param readOnly Read-only flag
	 */
	public IniFile(String path, boolean readOnly) {

		this.path = null;
		file = new TextFile(path);
		String currentCat = "";
		String line;
		HashMap<String, String> cat = new LinkedHashMap<String, String>();
		while ((line = file.readLine()) != null) {
			// Blank lines and comments
			if (line != "" && !line.startsWith(";")) {
				// Category change
				if (line.startsWith("[") && line.endsWith("]")) {
					// Appends the category to the map
					map.put(currentCat, cat);
					// Starts a new category
					currentCat = line.substring(1, line.length() - 1);
					cat = new LinkedHashMap<String, String>();
				} else {
					// Appends information to the category
					String[] split = line.split("=");
					if (split.length == 2) {
						cat.put(split[0].trim(), split[1].trim());
					}
				}
			}
		}
		// Appends the last category to the map
		map.put(currentCat, cat);
		this.path = path;
		this.readOnly = readOnly;

	}

	/** Close the parser, writing every values in the source file if it is not read-only. */
	public void close() {

		if (path != null && !readOnly) {
			file.clearLines();
			HashMap<String, String> m;
			String s;
			int cat = 0;
			for (String k1 : map.keySet()) {
				cat++;
				m = map.get(k1);
				if (k1 != "") {
					file.insertLine("[" + k1 + "]");
				}
				for (String k2 : m.keySet()) {
					s = m.get(k2);
					file.insertLine(k2 + " = " + s);
				}
				if (cat < map.keySet().size() && file.getCursor() > 0) {
					file.insertLine("");
				}
			}
			file.close();
		}

	}
	
	/**
	 * Returns whether or not the file is existing on disk or if a new file will be created.
	 * @return File existence flag
	 */
	public boolean exists() {
		return file.exists();
	}
	
	/** Deletes the file on disk. */
	public void delete() {
		file.delete();
	}
	
	/**
	 * Returns whether or not the category exists in the file.
	 * @param cat Category name
	 * @return Category existence flag
	 */
	public boolean containsCategory(String cat) {
		return map.containsKey(cat);
	}
	
	/**
	 * Returns whether of not the key exists in the file.
	 * @param cat Category name
	 * @param key Key name
	 * @return Key existence flag
	 */
	public boolean containsKey(String cat, String key) {
		if (map.containsKey(cat)) {
			HashMap<String, String> m = map.get(cat);
			if (m.containsKey(key)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the asked value with the given category and key.
	 * @param cat Value category
	 * @param key Value key
	 * @return Requested value, or null if no value has been found
	 */
	public String getValue(String cat, String key) {
		if (map.containsKey(cat)) {
			HashMap<String, String> m = map.get(cat);
			if (m.containsKey(key)) {
				return m.get(key);
			}
		}
		return null;
	}

	/**
	 * Returns the asked value converted to an integer with the given category and key.
	 * @param cat Value category
	 * @param key Value key
	 * @return Requested value, or 0 if the value has not been found.
	 */
	public int getIntValue(String cat, String key) {
		if (map.containsKey(cat)) {
			HashMap<String, String> m = map.get(cat);
			if (m.containsKey(key)) {
				return Integer.parseInt(m.get(key));
			}
		}
		return 0;
	}

	/**
	 * Returns the asked value converted to a float with the given category and key.
	 * @param cat Value category
	 * @param key Value key
	 * @return Requested value, or 0 if the value has not been found.
	 */
	public float getFloatValue(String cat, String key) {
		if (map.containsKey(cat)) {
			HashMap<String, String> m = map.get(cat);
			if (m.containsKey(key)) {
				return Float.parseFloat(m.get(key));
			}
		}
		return 0;
	}

	/**
	 * Sets the value with the given category and key to a new value. If the value doesn't exists, it will be created.
	 * @param cat Value category
	 * @param key Value key
	 * @param value Value
	 */
	public void setValue(String cat, String key, Object value) {
		HashMap<String, String> m;
		if (!map.containsKey(cat)) {
			m = new LinkedHashMap<String, String>();
			map.put(cat, m);
		} else {
			m = map.get(cat);
		}
		if (m.containsKey(key)) {
			m.remove(key);
		}
		m.put(key, value.toString());
	}

}
