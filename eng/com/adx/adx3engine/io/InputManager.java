package com.adx.adx3engine.io;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;

import org.lwjgl.BufferUtils;

import com.adx.adx3engine.frame.Game;
import com.adx.adx3engine.shapes.CPoint;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Input management system.
 * This class manages all keyboard keys, mouse buttons and position, as well as joystick axes and butttons.
 * It gives a better control over input by providing two new states: JUST_PRESSED and JUST_RELEASED.
 * Not only you can check whether a key or button is down or not, you can also check if it was just pressed or released on the current tick.
 * @author Alexandre Desbiens
 */
public class InputManager {

	private static final byte RELEASED = 0;
	private static final byte JUST_RELEASED = 1;
	private static final byte PRESSED = 2;
	private static final byte JUST_PRESSED = 3;

	private byte[] keyState = new byte[348];
	private byte[] buttonState = new byte[8];
	private JoystickStruct[] joystickState = null;
	private int mouseX = 0;
	private int mouseY = 0;
	private Game game;
	private String keyboardString = "";
	private int keyboardStringLimit = -1;
	private int keyboardStringIndex = 0;

	/* Constructors */
	
	public InputManager(Game game) {
		this.game = game;
	}
	
	/* Classes */
	
	/**
	 * Joystick informations structure.
	 * @author Alexandre Desbiens
	 */
	private class JoystickStruct {
		
		public String name;
		public float[] axes;
		public byte[] buttons;
		
	}
	
	/* Events */

	/**
	 * Launched when a keyboard key is pressed. It will then update the state of that key.
	 * It's value will also be appended to the keyboard string.
	 * @param key Key code
	 */
	public void keyPressed(int key) {
		if (key >= 0 && key < keyState.length) {
			keyState[key] = JUST_PRESSED;
			updateKeyboardString(key);
		}
	}

	/**
	 * Lauched when a keyboard key is released. It will then update the state of that key.
	 * @param key Key code
	 */
	public void keyReleased(int key) {
		if (key >= 0 && key < keyState.length) {
			keyState[key] = JUST_RELEASED;
		}
	}
	
	/**
	 * Launched when a key is kept pressed and key repeat starts.
	 * This will not put the state of the key to JUST_PRESSED, it will only write it's value to the keyboard string.
	 * @param key Key code
	 */
	public void keyRepeated(int key) {
		if (key >= 0 && key < keyState.length) {
			updateKeyboardString(key);
		}
	}
	
	public void buttonPressed(int key) {
		if (key >= 0 && key < buttonState.length) {
			buttonState[key] = JUST_PRESSED;
		}
	}
	
	public void buttonReleased(int key) {
		if (key >= 0 && key < buttonState.length) {
			buttonState[key] = JUST_RELEASED;
		}
	}

	/**
	 * Releases virtually the given keyboard key. The user will have to press that key again to trigger PRESSED.
	 * @param key Key code
	 */
	public void releaseKey(int key) {
		if (keyState[key] == JUST_RELEASED) {
			keyState[key] = RELEASED;
		} else if (keyState[key] == JUST_PRESSED) {
			keyState[key] = PRESSED;
		}
	}

	/**
	 * Releases virtually the given mouse button. The user will have to press that button again to trigger PRESSED.
	 * @param button
	 */
	public void releaseButton(int button) {
		if (buttonState[button] == JUST_RELEASED) {
			buttonState[button] = RELEASED;
		} else if (buttonState[button] == JUST_PRESSED) {
			buttonState[button] = PRESSED;
		}
	}
	
	/**
	 * Creates and initializes joysticks input.
	 */
	/*public void initJoysticks() {
		joystickState = new JoystickStruct[GLFW_JOYSTICK_LAST];
		for (int i = 0; i < GLFW_JOYSTICK_LAST; i++) {
			if (glfwJoystickPresent(GLFW_JOYSTICK_1 + i) == GL_TRUE) {
				joystickState[i] = new JoystickStruct();
				joystickState[i].name = glfwGetJoystickName(GLFW_JOYSTICK_1 + i);
				joystickState[i].axes = new float[glfwGetJoystickAxes(GLFW_JOYSTICK_1 + i).capacity()];
				joystickState[i].buttons = new byte[glfwGetJoystickButtons(GLFW_JOYSTICK_1 + i).capacity()];
			} else {
				joystickState[i] = null;
			}
		}
	}*/
	
	/* Update functions */
	
	/**
	 * Update the keyboard string by appending the given value to it. It will be added at the position of the keyboard string index.
	 * @param key Key code
	 */
	public void updateKeyboardString(int key) {
		if (key == Key.BACKSPACE && keyboardString.length() > 0 && keyboardStringIndex > 0) {
			keyboardString = keyboardString.substring(0, keyboardStringIndex - 1) + keyboardString.substring(keyboardStringIndex, keyboardString.length());
			keyboardStringIndex--;
		} else if (key == Key.RIGHT && keyboardStringIndex < keyboardString.length()) {
			keyboardStringIndex++;
		} else if (key == Key.LEFT && keyboardStringIndex > 0) {
			keyboardStringIndex--;
		}
	}

	/**
	 * Update all keys, buttons and joysticks (if applicable) states.
	 */
	public void updateButtons() {

		// Keyboard keys state
		for (int i = 0; i < keyState.length; i++) {

			if (keyState[i] == JUST_RELEASED) {
				keyState[i] = RELEASED;
			} else if (keyState[i] == JUST_PRESSED) {
				keyState[i] = PRESSED;
			}

		}

		// Mouse buttons state
		for (int i = 0; i < buttonState.length; i++) {

			if (buttonState[i] == JUST_RELEASED) {
				buttonState[i] = RELEASED;
			} else if (buttonState[i] == JUST_PRESSED) {
				buttonState[i] = PRESSED;
			}

		}
		
		// Joystick sticks and buttons state
		if (joystickState != null) {
			
			JoystickStruct s;
			for (int i = 0; i < GLFW_JOYSTICK_LAST; i++) {
				
				s = joystickState[i];
				if (s != null) {
					s.axes = glfwGetJoystickAxes(GLFW_JOYSTICK_1 + i).array();
					ByteBuffer buffer = glfwGetJoystickButtons(GLFW_JOYSTICK_1 + i);
					byte b;
					for (int j = 0; j < s.buttons.length; j++) {
						
						b = buffer.get();
						if (b == 1) {
							if (s.buttons[j] == JUST_RELEASED || s.buttons[j] == RELEASED) {
								s.buttons[j] = JUST_PRESSED;
							} else {
								s.buttons[j] = PRESSED;
							}
						} else {
							if (s.buttons[j] == JUST_PRESSED || s.buttons[j] == PRESSED) {
								s.buttons[j] = JUST_RELEASED;
							} else {
								s.buttons[j] = RELEASED;
							}
						}
						
					}
				}
				
			}
			
		}

	}

	/**
	 * Updates the mouse position.
	 * The final values are the mouse position scaled to the current viewport.
	 * @param window
	 */
	public void updateMousePosition(long window, float renderX, float renderY, float renderWidthRatio, float renderHeightRatio) {

		DoubleBuffer x = BufferUtils.createDoubleBuffer(1);
		DoubleBuffer y = BufferUtils.createDoubleBuffer(1);
		glfwGetCursorPos(window, x, y);
		x.rewind();
		y.rewind();
		mouseX = (int) ((x.get() - renderX) / renderWidthRatio);
		mouseY = (int) ((y.get() - renderY) / renderHeightRatio);

	}
	
	/* Keyboard string functions */

	/**
	 * Returns the keyboard string built from every input on the keyboard.
	 * @return Keyboard string
	 */
	public String getKeyboardString() {
		return keyboardString;
	}

	/**
	 * Sets the keyboard string to the given string.
	 * @param str New keyboard string
	 */
	public void setKeyboardString(String str) {
		keyboardString = str;
		keyboardStringIndex = str.length();
	}

	/**
	 * Adds a character to the keyboard string. It will be place at the position of the keyboard string index.
	 * @param c Character code
	 */
	public void addCharToKeyboardString(char c) {
		if (keyboardStringLimit == - 1 || keyboardString.length() < keyboardStringLimit) {
			keyboardString = keyboardString.substring(0, keyboardStringIndex) + c + keyboardString.substring(keyboardStringIndex, keyboardString.length());
			keyboardStringIndex++;
		}
	}
	
	/**
	 * Returns the current position of the cursor in the keyboard string.
	 * @return
	 */
	public int getKeyboardStringIndex() {
		return keyboardStringIndex;
	}
	
	/**
	 * Sets a limit to the keyboard string's size.
	 * @param limit Size limit, in characters
	 */
	public void setKeyboardStringLimit(int limit) {
		keyboardStringLimit = limit;
	}

	/* Keyboard keys functions */
	
	/**
	 * Checks if the given key is down.
	 * @param key Key code
	 * @return True if the key is down, false otherwise
	 */
	public boolean getKey(int key) {
		return keyState[key] == PRESSED || keyState[key] == JUST_PRESSED;
	}

	/**
	 * Checks if the given key was just pressed this tick.
	 * @param key Key code
	 * @return True if the key was just pressed, false otherwise
	 */
	public boolean getKeyPressed(int key) {
		return keyState[key] == JUST_PRESSED;
	}

	/**
	 * Checks if the given key was just released this tick.
	 * @param key Key code
	 * @return True if the key was just released, false otherwise
	 */
	public boolean getKeyReleased(int key) {
		return keyState[key] == JUST_RELEASED;
	}
	
	/**
	 * Virtually disables the keybord, releasing all keys.
	 * If a key was down, it will need to be released and pressed again.
	 */
	public void disableKeyboard() {
		for (int i = 0; i < keyState.length; i++) {
			keyState[i] = RELEASED;
		}
	}
	
	/* Mouse functions */

	/**
	 * Checks if the given mouse button is down.
	 * @param button Button code
	 * @return True if the button is down, false otherwise
	 */
	public boolean getButton(int button) {
		return buttonState[button] == PRESSED || buttonState[button] == JUST_PRESSED;
	}

	/**
	 * Checks if the given mouse button was just pressed this tick.
	 * @param button Button code
	 * @return True if the button was just pressed, false otherwise
	 */
	public boolean getButtonPressed(int button) {
		return buttonState[button] == JUST_PRESSED;
	}

	/**
	 * Checks if the given mouse button was just released this tick.
	 * @param button Button code
	 * @return True if the button was just released, false otherwise
	 */
	public boolean getButtonReleased(int button) {
		return buttonState[button] == JUST_RELEASED;
	}

	/**
	 * Returns the mouse position on the X axis, scaled to the viewport.
	 * @return Scaled mouse X
	 */
	public int getMouseX() {
		return mouseX;
	}

	/**
	 * Returns the mouse position on the Y axis, scaled to the viewport.
	 * @return Scaled mouse Y
	 */
	public int getMouseY() {
		return mouseY;
	}
	
	public CPoint getMousePosition() {
		return new CPoint(mouseX, mouseY);
	}

	/**
	 * Sets the mouse position in the screen. This is not scaled to the viewport.
	 * @param x New mouse X position
	 * @param y New mouse Y position
	 */
	public void setMousePosition(int x, int y) {
		glfwSetCursorPos(game.getWindow(), x, y);
		mouseX = x;
		mouseY = y;
	}

	/**
	 * Toggles the visibility of the mouse cursor.
	 * @param visible Mouse cursor visibility
	 */
	public void setMouseVisible(boolean visible) {
		if (visible) {
			glfwSetInputMode(game.getWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		} else {
			glfwSetInputMode(game.getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}
	}
	
	/**
	 * Virtually disables the mouse position by placing it in a far location.
	 */
	public void disableMousePosition() {
		mouseX = Integer.MIN_VALUE;
		mouseY = Integer.MIN_VALUE;
	}
	
	/**
	 * Virtually disables the mouse position and buttons.
	 * If a mouse button was down, it will need to be released and pressed again.
	 */
	public void disableMouse() {
		disableMousePosition();
		for (int i = 0; i < buttonState.length; i++) {
			buttonState[i] = RELEASED;
		}
	}
	
	/* Joystick functions */
	
	/**
	 * Returns the name of the given joystick.
	 * @param joystick Joystick index
	 * @return Name of the joystick
	 */
	public String getJoystickName(int joystick) {
		if (joystickState != null && joystick >= GLFW_JOYSTICK_1 && joystick <= GLFW_JOYSTICK_LAST ) {
			JoystickStruct s = joystickState[joystick];
			if (s != null) {
				return s.name;
			}
		}
		return null;
	}
	
	/**
	 * Returns the state of the given axis.
	 * @param joystick Joystick index
	 * @param axes Axis index
	 * @return Axis state, between -1.0f and 1.0f
	 */
	public float getJoystickAxis(int joystick, int axes) {
		if (joystickState != null && joystick >= GLFW_JOYSTICK_1 && joystick <= GLFW_JOYSTICK_LAST ) {
			JoystickStruct s = joystickState[joystick];
			if (s != null && axes >= 0 && axes < s.axes.length) {
				return s.axes[axes];
			}
		}
		return 0.0f;
	}
	
	/**
	 * Checks if the given button is down.
	 * @param joystick Joystick index
	 * @param button Button index
	 * @return True if the button is down, false otherwise
	 */
	public boolean getJoystickButton(int joystick, int button) {
		if (joystickState != null && joystick >= GLFW_JOYSTICK_1 && joystick <= GLFW_JOYSTICK_LAST ) {
			JoystickStruct s = joystickState[joystick];
			if (s != null && button >= 0 && button < s.buttons.length) {
				return s.buttons[button] == PRESSED || s.buttons[button] == JUST_PRESSED;
			}
		}
		return false;
	}
	
	/**
	 * Checks if the given button was just pressed this tick.
	 * @param joystick Joystick index
	 * @param button Button index
	 * @return True if the button was just pressed, false otherwise
	 */
	public boolean getJoystickButtonPressed(int joystick, int button) {
		if (joystickState != null && joystick >= GLFW_JOYSTICK_1 && joystick <= GLFW_JOYSTICK_LAST ) {
			JoystickStruct s = joystickState[joystick];
			if (s != null && button >= 0 && button < s.buttons.length) {
				return s.buttons[button] == JUST_PRESSED;
			}
		}
		return false;
	}
	
	/**
	 * Checks if the given button was just released this tick.
	 * @param joystick Joystick index
	 * @param button Button index
	 * @return True if the button was just released, false otherwise
	 */
	public boolean getJoystickButtonReleased(int joystick, int button) {
		if (joystickState != null && joystick >= GLFW_JOYSTICK_1 && joystick <= GLFW_JOYSTICK_LAST ) {
			JoystickStruct s = joystickState[joystick];
			if (s != null && button >= 0 && button < s.buttons.length) {
				return s.buttons[button] == JUST_RELEASED;
			}
		}
		return false;
	}

}
