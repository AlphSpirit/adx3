package com.adx.adx3engine.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// TODO - Enlever le suppress warning
@SuppressWarnings("unused")
public class JsonFile {
	
	private List<Cluster> clusterList = new ArrayList<Cluster>();
	
	public JsonFile(String path) {
		
		TextFile textFile = new TextFile(path);
		if (textFile.exists()) {
			String content = textFile.readAll();
			String[] split = content.split("[,\\{\\}]");
			for (int i = 0; i < split.length; i++) {
				split[i] = split[i].trim().replace("\r\n", "");
			}
			List<String> splitList = new ArrayList<String>(Arrays.asList(split));
			System.out.println(splitList);
			Cluster cluster;
			while (splitList.size() > 0) {
				cluster = findCluster(splitList);
				if (cluster != null) {
					clusterList.add(cluster);
				}
			}
			System.out.println(clusterList.size());
		}
		
	}
	
	private class Cluster {
		private String variable;
		private String value;
		private List<Cluster> embeded = new ArrayList<Cluster>();
		public Cluster(String variable) {
			this.variable = variable;
		}
		public void addEmbededCluster(Cluster cluster) {
			embeded.add(cluster);
		}
		public void setValue(String value) {
			this.value = value;
		}
	}
	
	private Cluster findCluster(List<String> splitList) {
		String line = extractLine(splitList);
		if (line.endsWith(":")) {
			//System.out.println("New cluster found with root: " + line);
			Cluster cluster = new Cluster(line);
			Cluster embeded;
			while (true) {
				embeded = findCluster(splitList);
				if (embeded != null) {
					cluster.embeded.add(embeded);
				} else {
					cluster.setValue(extractLine(splitList));;
					break;
				}
			}
			return cluster;
		}
		return null;
	}
	
	private String extractLine(List<String> splitList) {
		String line = splitList.get(0);
		splitList.remove(0);
		return line;
	}

}
