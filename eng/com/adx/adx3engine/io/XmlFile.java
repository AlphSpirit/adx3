package com.adx.adx3engine.io;

import java.util.ArrayList;

public class XmlFile {
	
	private String path;
	//private boolean readOnly = true;
	private Node master = new Node("***MASTER***");
	
	public class Node {
		public String name;
		public ArrayList<String> data = new ArrayList<String>();
		public ArrayList<Node> nodes = new ArrayList<Node>();
		public Node(String name) {
			this.name = name;
		}
		public Node getNode(String name) {
			for (Node n : nodes) {
				if (n.name.equals(name)) {
					return n;
				}
			}
			return null;
		}
	}
	
	private class Lines {
		public String[] lines;
		public int index = 1;
		public Lines(String[] lines) {
			this.lines = lines;
		}
		public String getLine() {
			return lines[index];
		}
		public void setLine(String str) {
			lines[index] = str;
		}
		public void inc() {
			index++;
		}
	}
	
	public XmlFile(String path, boolean readOnly) {
		
		TextFile txt = new TextFile(path);
		String[] lines = txt.getLines();
		for (int i = 0; i < lines.length; i++) {
			lines[i] = lines[i].trim();
		}
		readNode(new Lines(lines), master);
		
	}
	
	public String getPath() {
		return path;
	}
	
	public Node getMasterNode() {
		return master;
	}
	
	public Node readNode(Lines lines, Node parent) {
		
		boolean oneLiner = false;
		if (lines.getLine().endsWith("/>")) {
			oneLiner = true;
			lines.setLine(lines.getLine().substring(1, lines.getLine().length() - 2));
		} else {
			lines.setLine(lines.getLine().substring(1, lines.getLine().length() - 1));
		}
		String[] split = lines.getLine().split(" ");
		Node n = new Node(split[0]);
		lines.inc();
		if (!oneLiner) {
			while (lines.index < lines.lines.length) {
				if (lines.getLine().startsWith("</" + n.name)) {
					lines.inc();
					break;
				} else if (lines.getLine().startsWith("<")) {
					readNode(lines, n);
				} else {
					n.data.add(lines.getLine());
					lines.inc();
				}
			}
		}
		parent.nodes.add(n);
		return n;
		
	}
	
	@Override
	public String toString() {
		return super.toString();
	}

}
