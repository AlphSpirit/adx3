package com.adx.adx3engine.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Text file buffered reader.
 * It reads the whole content of the text file and places it in an internal buffer to accelerate value modifications and reading.
 * Can be used to create a file on disk.
 * @author Alexandre Desbiens
 */
public class TextFile {

	private List<String> lText = new ArrayList<String>();
	private int cursor = 0;
	private String path = null;
	private boolean readOnly = false;
	private boolean justCreated = true;
	private boolean exists = false;
	private long lastModified = -1;

	/**
	 * Initializes the reader with the path to the source file
	 * @param path Path to source file
	 */
	public TextFile(String path) {
		this(path, false);
	}
	
	/**
	 * Initializes the read with the path to the file to read and a read-only flag
	 * @param path Path to source file
	 * @param readOnly Read-only flag
	 */
	private TextFile(String path, boolean readOnly) {
		
		// Look for the file on the disk
		File f = new File(path);
		if (f.exists()) {
			BufferedReader br;
			try {
				br = new BufferedReader(new FileReader(path));
				String str;
				while ((str = br.readLine()) != null) {
					lText.add(str);
				}
				br.close();
				exists = true;
				justCreated = false;
				lastModified = f.lastModified();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			// Look for it in the class path
			try {
				InputStream is = getClass().getResourceAsStream(path);
				if (is != null) {
					BufferedReader br = new BufferedReader(new InputStreamReader(is));
					if (br != null) {
						String str;
						while ((str = br.readLine()) != null) {
							lText.add(str);
						}
						br.close();
						exists = true;
						justCreated = false;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		this.path = path;
		this.readOnly = readOnly;
		
	}
	
	/**
	 * Returns true if the file exists on disk.
	 * @return File existence flag
	 */
	public boolean exists() {
		return exists;
	}
	
	/**
	 * Return the last modification date on a long format.
	 * @return Last modification date
	 */
	public long getLastModified() {
		return lastModified;
	}
	
	/**
	 * Delete a file on disk.
	 * @param path Path to the file to delete
	 */
	public static void delete(String path) {
		File f = new File(path);
		f.delete();
	}
	
	/**
	 * Return the current line position of the cursor.
	 * @return Cursor line position
	 */
	public int getCursor() {
		return cursor;
	}

	/**
	 * Read a line of the text file and increments the cursor.
	 * @return String read, or null if the end of the file has been reached
	 */
	public String readLine() {
		if (cursor >= lText.size()) {
			return null;
		}
		String str = lText.get(cursor);
		cursor++;
		return str;
	}

	/**
	 * Read the line of text at the given line index.
	 * @param index Line index
	 * @return String read
	 */
	public String readLine(int index) {
		if (index >= 0 && index < lText.size()) {
			return lText.get(index);
		}
		return null;
	}
	
	/**
	 * Read all the lines of the file and appends them to one string.
	 * @return String containing all text file
	 */
	public String readAll() {
		StringBuilder builder = new StringBuilder();
		for (String s : lText) {
			builder.append(s);
			if (s != lText.get(lText.size() - 1)) {
				builder.append("\r\n");
			}
		}
		return builder.toString();
	}
	
	public String[] getLines() {
		String[] str = new String[lText.size()];
		for (int i = 0; i < str.length; i++) {
			str[i] = lText.get(i);
		}
		return str;
	}
	
	/** Clears the text lines. */
	public void clearLines() {
		lText.clear();
		cursor = 0;
	}
	
	/**
	 * Write the given string to the current line index, modifying its content.
	 * @param str Text to write
	 */
	public void writeLine(String str) {
		if (cursor >= lText.size()) {
			return;
		}
		lText.remove(cursor);
		lText.add(cursor, str);
		cursor++;
	}

	/**
	 * Write the given string to the given line index, modifying the value.
	 * @param index Line index
	 * @param str Text to write
	 */
	public void writeLine(int index, String str) {
		if (index >= 0 && index < lText.size()) {
			lText.remove(index);
			lText.add(index, str);
		}
	}
	
	/**
	 * Inserts a line at the current cursor line index.
	 * @param str Text to insert
	 */
	public void insertLine(String str) {
		lText.add(cursor, str);
		cursor++;
	}
	
	/**
	 * Inserts a line at the given line index.
	 * @param index Line index
	 * @param str Text to insert
	 */
	public void insertLine(int index, String str) {
		if (index >= 0 && index <= lText.size()) {
			lText.add(index, str);
		}
	}

	/** Closes the reader, writing all lines into the source file if it not read-only. */
	public void close() {
		if (path != null && !readOnly) {
			try {
				if (justCreated) {
					File f = new File(path);
					f.getParentFile().mkdirs();
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(path));
				for (String str : lText) {
					bw.write(str);
					bw.write("\r\n");
				}
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/** Deletes the current file. */
	public void delete() {
		TextFile.delete(path);
	}

}
