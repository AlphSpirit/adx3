package com.adx.adx3engine.frame;

import com.adx.adx3engine.io.InputManager;
import com.adx.adx3engine.render.GraphicsRenderer;
import com.adx.adx3engine.render.View;

public abstract class Instance {
	
	private int roomID;
	protected int depth;
	private InstanceList<?> instanceList = null;
	
	public Instance() {
		this(-1, 0);
	}
	
	public Instance(int roomID, int depth) {
		this.roomID = roomID;
		this.depth = depth;
	}
	
	public int getRoomID() {
		return roomID;
	}
	
	public void setRoomID(int id) {
		roomID = id;
	}
	
	public int getDepth() {
		return depth;
	}
	
	public void setDepth(Game game, int depth) {
		this.depth = depth;
		game.changeInstanceDepth(this);
	}
	
	public void setInstanceList(InstanceList<?> instanceList) {
		this.instanceList = instanceList;
	}

	public abstract void load(Game game);
	public abstract void update(Game game, InputManager input);
	public abstract void render(Game game, GraphicsRenderer g, View view);
	
	public void destroy(Game game) {
		if (instanceList != null) {
			instanceList.removeInstance(game, this);
		} else {
			game.removeInstance(this);
		}
	}
	
}
