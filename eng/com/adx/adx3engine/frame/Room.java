package com.adx.adx3engine.frame;

import com.adx.adx3engine.io.InputManager;
import com.adx.adx3engine.render.GraphicsRenderer;
import com.adx.adx3engine.render.View;

public abstract class Room {
	
	private int roomID;
	
	public Room(int roomID) {
		this.roomID = roomID;
	}
	
	public int getID() {
		return roomID;
	}
	
	public abstract void load(Game game);
	public abstract void update(Game game, InputManager input);
	public void endUpdate(Game game, InputManager input) {
	}
	public abstract void render(Game game, GraphicsRenderer g, View view);
	public void endRender(Game game, GraphicsRenderer g, View view) {
	}
	public void resize(Game game) {
	}
	public void exit(Game game) {
	}

}
