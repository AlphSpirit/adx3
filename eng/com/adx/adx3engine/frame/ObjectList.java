package com.adx.adx3engine.frame;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.adx.adx3engine.maths.ListUtils;

public abstract class ObjectList<T> {
	
	private List<T> lInstance;
	private ListUtils<T> lUtils;
	
	public ObjectList() {
		lInstance = new ArrayList<T>();
		lUtils = new ListUtils<T>();
	}
	
	/**
	 * Returns the object list.
	 * @return Object list
	 */
	public List<T> getInstanceList() {
		return lInstance;
	}
	
	public int getCount() {
		return lInstance.size();
	}
	
	/**
	 * Returns the ListUtils linked to the object list.
	 * @return Linked ListUtils
	 */
	public ListUtils<T> getListUtils() {
		return lUtils;
	}
	
	/**
	 * Tests all objects in list against the given function.
	 * @param f Testing function
	 * @return True if any of the objects matched the given function, false otherwise
	 */
	public boolean test(Function<T, Boolean> f) {
		return lUtils.test(lInstance, f);
	}
	
	/**
	 * Adds an object to the object list.
	 * @param instance Object to add
	 */
	public void addInstance(T instance) {
		lInstance.add(instance);
	}
	
	/**
	 * Removes an object at the given index.
	 * @param index Index
	 */
	public T removeInstance(int index) {
		return lInstance.remove(index);
	}
	
	/**
	 * Removes a given object from the object list.
	 * @param instance Object to remove
	 */
	public void removeInstance(Object instance) {
		lInstance.remove(instance);
	}
	
	public ObjectList<T> clear() {
		lInstance.clear();
		return this;
	}

}
