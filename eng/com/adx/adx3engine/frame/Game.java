package com.adx.adx3engine.frame;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.Sys;
import org.lwjgl.glfw.Callbacks;
import org.lwjgl.glfw.GLFWCharCallback;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.glfw.GLFWvidmode;
import org.lwjgl.openal.ALContext;
import org.lwjgl.openal.ALDevice;
import org.lwjgl.opengl.GL;

import com.adx.adx3engine.io.InputManager;
import com.adx.adx3engine.render.GraphicsRenderer;
import com.adx.adx3engine.render.OpenGLRenderer;
import com.adx.adx3engine.render.View;

public class Game {

	public static final byte STRETCH_NONE = 0;
	public static final byte STRETCH_FULL = 1;
	public static final byte STRETCH_SMART = 2;
	public static final byte STRETCH_FILL = 3;
	public static final byte STRETCH_RESIZE = 4;

	public static final byte GRAPHICS_DEFAULT_ID = -1;
	public static final byte VIEW_DEFAULT_ID = -1;

	private boolean run = false;
	private long window = NULL;
	private boolean fullscreen = false;
	private boolean recreateWindow = false;
	private GLFWErrorCallback errorCallback;
	private WindowSizeCallback windowSizeCallback;
	private GLFWKeyCallback keyCallback;
	private GLFWCharCallback charCallback;
	private GLFWMouseButtonCallback buttonCallback;
	private UpdateThread updateThread;
	private int updateTicks;
	private int renderTicks;
	private boolean skipUpdate = false;

	private int gameWidth;
	private int gameHeight;
	private int windowWidth;
	private int windowHeight;
	private int originalGameWidth;
	private int originalGameHeight;
	private int originalWindowWidth;
	private int originalWindowHeight;
	private byte stretchMode = STRETCH_SMART;
	private float renderX;
	private float renderY;
	private float renderWidthRatio;
	private float renderHeightRatio;

	private List<GraphicsRenderer> lGraphics;
	private List<View> lView;
	private List<Room> lRoom;
	private List<Instance> lInstance;

	private Room currentRoom = null;
	private Room roomToLoad = null;
	private Room roomToUnload = null;
	private List<Instance> lInstanceToLoad;
	private InputManager input;

	private ALDevice audioDevice;
	private ALContext audioContext;

	/* Constructors */

	public Game() {
		this(1024, 768, 1024, 768);
	}

	public Game(int width, int height) {
		this(width, height, width, height);
	}

	public Game(int width, int height, int windowWidth, int windowHeight) {
		this.gameWidth = width;
		this.gameHeight = height;
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;
		originalGameWidth = windowWidth;
		originalGameHeight = windowHeight;
		originalWindowWidth = windowWidth;
		originalWindowHeight = windowHeight;
		lGraphics = new ArrayList<GraphicsRenderer>();
		lView = new ArrayList<View>();
		lRoom = new ArrayList<Room>();
		lInstance = new ArrayList<Instance>();
		lInstanceToLoad = new ArrayList<Instance>();
		input = new InputManager(this);
	}

	/* Accessors and mutators */

	public int getWidth() {
		return gameWidth;
	}

	public int getHeight() {
		return gameHeight;
	}

	public byte getStretchMode() {
		return stretchMode;
	}

	public void setStretchMode(byte strechMode) {
		this.stretchMode = strechMode;
		calculateRenderInformations();
	}

	public boolean getFullscreen() {
		return fullscreen;
	}

	public void setFullscreen(boolean fullscreen) {

		if (this.fullscreen == fullscreen) {
			return;
		}
		this.fullscreen = fullscreen;
		if (window != NULL) {
			recreateWindow = true;
		}

	}

	public long getWindow() {
		return window;
	}

	public Room getCurrentRoom() {
		return currentRoom;
	}

	/* Classes and callbacks */

	private class UpdateThread extends Thread {

		private Game game;

		public UpdateThread(Game game) {
			this.game = game;
		}

		@Override
		public void run() {

			double timeThen = glfwGetTime();
			double timeNow;
			double accum = 0;
			int second = 0;
			double interval = 1.0 / 60;

			while (run) {
				timeNow = glfwGetTime();
				accum += (timeNow - timeThen);
				if (accum > interval) {
					input.updateMousePosition(window, renderX, renderY, renderWidthRatio, renderHeightRatio);
					synchronized (lInstance) {
						currentRoom.update(game, input);
						Instance instance;
						for (int i = lInstance.size() - 1; i >= 0; i--) {
							if (skipUpdate) {
								skipUpdate = false;
								break;
							}
							if (i < lInstance.size()) {
								instance = lInstance.get(i);
								if (instance.getRoomID() == currentRoom.getID() || instance.getRoomID() == -1) {
									instance.update(game, input);
								}
							}
						}
						currentRoom.endUpdate(game, input);
					}
					input.updateButtons();
					updateTicks++;
					accum = 0;
				}
				while ((int) timeNow > second) {
					printString("UP: " + updateTicks + " RE: " + renderTicks + " IN:" + lInstance.size() + " GR:"
							+ lGraphics.size());
					second++;
					updateTicks = 0;
					renderTicks = 0;
				}
				timeThen = timeNow;
			}

		}

	}

	private class WindowSizeCallback extends GLFWWindowSizeCallback {
		private Game game;
		public WindowSizeCallback(Game game) {
			this.game = game;
		}
		@Override
		public void invoke(long window, int width, int height) {
			if (width <= 0 || height <= 0) {
				return;
			}
			if (windowWidth == width && windowHeight == height) {
				return;
			}
			windowWidth = width;
			windowHeight = height;
			if (stretchMode == STRETCH_RESIZE) {
				gameWidth = width;
				gameHeight = height;
				currentRoom.resize(game);
			}
			calculateRenderInformations();
		}
	}

	private class KeyboardCallback extends GLFWKeyCallback {
		@Override
		public void invoke(long window, int key, int scancode, int action, int mods) {
			if (action == GLFW_PRESS) {
				input.keyPressed(key);
			} else if (action == GLFW_RELEASE) {
				input.keyReleased(key);
			} else if (action == GLFW_REPEAT) {
				input.keyRepeated(key);
			}
		}
	}

	private class KeyboardCharCallback extends GLFWCharCallback {
		@Override
		public void invoke(long window, int codepoint) {
			input.addCharToKeyboardString((char) codepoint);
		}
	}

	private class MouseButtonCallback extends GLFWMouseButtonCallback {
		@Override
		public void invoke(long window, int button, int action, int mods) {
			if (action == GLFW_PRESS) {
				input.buttonPressed(button);
			} else if (action == GLFW_RELEASE) {
				input.buttonReleased(button);
			}
		}
	}

	/* Core functions */

	public void start() {

		printString("*** ADX ENGINE v3 BOOTING UP ***");
		printString("LWJGL3 version: " + Sys.getVersion());

		if (initialize()) {

			render();
			run = false;

		}

		printString("");
		printString("*** ADX ENGINE v3 SHUTTING DOWN ***");
		terminate();

	}

	public void render() {

		GraphicsRenderer.createWhiteTexture(this);
		
		calculateRenderInformations();

		while (run && glfwWindowShouldClose(window) == GL_FALSE) {

			glClear(GL_COLOR_BUFFER_BIT);

			if (recreateWindow) {
				recreateWindow = false;
				createWindow();
			}

			synchronized (lInstance) {

				if (lInstanceToLoad.size() > 0) {
					Instance instance;
					while (lInstanceToLoad.size() > 0) {
						instance = lInstanceToLoad.get(0);
						addInstanceToList(instance);
						instance.load(this);
						lInstanceToLoad.remove(0);
					}
				}
				if (roomToUnload != null) {
					roomToUnload.exit(this);
					roomToUnload = null;
				}
				if (roomToLoad != null) {
					currentRoom = roomToLoad;
					roomToLoad.load(this);
					roomToLoad = null;
				}

				for (GraphicsRenderer g : lGraphics) {
					for (View v : lView) {
						g.init(setViewScreenLocation(v));
						g.startDraw();
						currentRoom.render(this, g, v);
						for (Instance instance : lInstance) {
							if (instance.getRoomID() == currentRoom.getID() || instance.getRoomID() == -1) {
								instance.render(this, g, v);
							}
						}
						currentRoom.endRender(this, g, v);
						g.endDraw();
					}
				}

			}
			
			glfwSwapBuffers(window);
			glfwPollEvents();
			renderTicks++;

		}

	}

	public void stop() {

		run = false;

	}

	private boolean initialize() {

		printString("Verifying starting room...");
		if (currentRoom == null) {
			printFatal("A game must have at least one room!");
			return false;
		}

		printString("Initializing GLFW...");
		if (glfwInit() != GL_TRUE) {
			printFatal("Could not start GLFW!");
			return false;
		}

		printString("Creating callbacks...");
		errorCallback = Callbacks.errorCallbackPrint(System.out);
		glfwSetErrorCallback(errorCallback);
		windowSizeCallback = new WindowSizeCallback(this);
		keyCallback = new KeyboardCallback();
		charCallback = new KeyboardCharCallback();
		buttonCallback = new MouseButtonCallback();

		createWindow();

		printString("Verifying graphics settings...");
		if (lGraphics.size() == 0) {
			lGraphics.add(new OpenGLRenderer(-1));
		}

		printString("Verifying views...");
		if (lView.size() == 0) {
			lView.add(createDefaultView());
		}

		printString("Opening OpenAL devices...");
		audioDevice = ALDevice.create(null);

		printString("Creating OpenAL context...");
		audioContext = ALContext.create(audioDevice);

		printString("Starting Update thread...");
		run = true;
		updateThread = new UpdateThread(this);
		updateThread.start();

		printString("Game started successfully.");
		printString("");
		return true;

	}

	private boolean createWindow() {

		printString("Creating GLFW window...");
		if (fullscreen) {
			ByteBuffer vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
			windowWidth = GLFWvidmode.width(vidMode);
			windowHeight = GLFWvidmode.height(vidMode);
			if (stretchMode == STRETCH_RESIZE) {
				gameWidth = windowWidth;
				gameHeight = windowHeight;
			}
		} else {
			windowWidth = originalWindowWidth;
			windowHeight = originalWindowHeight;
			if (stretchMode == STRETCH_RESIZE) {
				gameWidth = originalGameWidth;
				gameHeight = originalGameHeight;
			}
		}
		if (window != NULL && currentRoom != null && stretchMode == STRETCH_RESIZE) {
			currentRoom.resize(this);
		}
		long newWindow = glfwCreateWindow(windowWidth, windowHeight, "", fullscreen ? glfwGetPrimaryMonitor() : NULL,
				window);
		if (newWindow == NULL) {
			glfwTerminate();
			printFatal("Could not create GLFW window!");
			return false;
		}
		if (window != NULL) {
			glfwDestroyWindow(window);
		}
		window = newWindow;

		printString("Binding OpenGL context to window...");
		glfwMakeContextCurrent(window);
		glfwSwapInterval(0);
		GL.createCapabilities();

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_TEXTURE_2D);

		glfwSetWindowSizeCallback(window, windowSizeCallback);
		glfwSetKeyCallback(window, keyCallback);
		glfwSetCharCallback(window, charCallback);
		glfwSetMouseButtonCallback(window, buttonCallback);

		return true;

	}

	private void terminate() {

		printString("Waiting for update thread to shut down...");
		if (updateThread != null) {
			try {
				updateThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		currentRoom.exit(this);

		printString("Terminating GLFW...");
		glfwTerminate();

		printString("Releasing audio devices...");
		audioContext.destroy();
		audioDevice.destroy();

		printString("Releasing callbacks...");
		if (errorCallback != null) {
			errorCallback.release();
		}

		printString("Game stopped successfully.");

	}

	/* Rooms */

	public boolean addRoom(Room room) {
		for (Room r : lRoom) {
			if (r.getID() == room.getID()) {
				printError("Could not add room with id " + room.getID()
						+ " because another room with the same id already exists.");
				return false;
			}
		}
		lRoom.add(room);
		if (currentRoom == null) {
			currentRoom = room;
			roomToLoad = room;
		}
		return true;
	}
	
	public Room getRoom(int roomID) {
		for (Room r : lRoom) {
			if (r.getID() == roomID) {
				return r;
			}
		}
		return null;
	}

	public boolean changeRoom(int roomID) {
		for (Room r : lRoom) {
			if (r.getID() == roomID) {
				synchronized (lInstance) {
					roomToUnload = currentRoom;
					Instance instance;
					for (int i = lInstance.size() - 1; i >= 0; i--) {
						instance = lInstance.get(i);
						if (instance.getRoomID() != roomID && instance.getRoomID() != -1) {
							instance.destroy(this);
							removeInstance(instance);
						}
					}
					View v = getView(VIEW_DEFAULT_ID);
					if (v != null) {
						v.setPosition(0, 0);
					}
					roomToLoad = r;
					skipUpdate = true;
					return true;
				}
			}
		}
		printError("Could not move to room with id " + roomID + " because it does not exist.");
		return false;
	}

	/* Instances */

	public void addInstance(Instance instance) {
		if (instance.getRoomID() == -1) {
			instance.setRoomID(currentRoom.getID());
		}
		/*if (inRenderThread) {
			addInstanceToList(instance);
			instance.load(this);
		} else {*/
			lInstanceToLoad.add(instance);
		//}
	}

	private void addInstanceToList(Instance instance) {
		boolean added = false;
		synchronized (lInstance) {
			for (int i = 0; i < lInstance.size(); i++) {
				if (lInstance.get(i).getDepth() < instance.getDepth()) {
					added = true;
					lInstance.add(i, instance);
					break;
				}
			}
			if (!added) {
				lInstance.add(instance);
			}
		}
	}

	public boolean removeInstance(Instance instance) {
		synchronized(lInstance) {
			if (!lInstance.remove(instance)) {
				return false;
			}
		}
		return true;
	}
	
	public boolean changeInstanceDepth(Instance instance) {
		lInstance.remove(instance);
		addInstanceToList(instance);
		return true;
	}

	/* Views */

	public boolean addView(View v) {
		if (getView(v.getID()) != null) {
			printError("Could not add view with id " + v.getID() + " because another view with this id exists.");
			return false;
		}
		lView.add(v);
		return true;
	}

	public View getView(int id) {
		for (View v : lView) {
			if (v.getID() == id) {
				return v;
			}
		}
		return null;
	}

	public boolean removeView(int id) {
		View v = getView(id);
		if (v == null) {
			printWarning("Could not remove view with id " + id + " because it does not exist.");
			return false;
		}
		lView.remove(v);
		if (lView.size() == 0) {
			addView(createDefaultView());
		}
		return true;
	}

	public boolean clearViews() {
		if (lView.size() == 0) {
			printWarning("Cannot clear views because there is none.");
			return false;
		}
		lView.clear();
		return true;
	}

	/* Graphics renderer */

	public void addGraphics(GraphicsRenderer g) {
		lGraphics.add(g);
	}
	
	public GraphicsRenderer getGraphics(int id) {
		for (GraphicsRenderer g : lGraphics) {
			if (g.getID() == id) {
				return g;
			}
		}
		return null;
	}

	public void removeGraphics(int id) {
		GraphicsRenderer g;
		for (int i = lGraphics.size() - 1; i >= 0; i--) {
			g = lGraphics.get(i);
			if (g.getID() == id) {
				lGraphics.remove(g);
			}
		}
	}

	/* Utility functions */

	private View createDefaultView() {
		return new View(-1, 0, 0, gameWidth, gameHeight, 0, 0, gameWidth, gameHeight);
	}
	
	private void calculateRenderInformations() {
		switch (stretchMode) {
		case STRETCH_NONE:
			renderX = (windowWidth - gameWidth) / 2;
			renderY = (windowHeight - gameHeight) / 2;
			renderWidthRatio = 1;
			renderHeightRatio = 1;
			break;
		case STRETCH_FULL:
			renderX = 0;
			renderY = 0;
			renderWidthRatio = windowWidth / (float) gameWidth;
			renderHeightRatio = windowHeight / (float) gameHeight;
			break;
		case STRETCH_SMART:
			float xScaleScreen = windowWidth / (float) gameWidth;
			float yScaleScreen = windowHeight / (float) gameHeight;
			float smallest = xScaleScreen < yScaleScreen ? xScaleScreen : yScaleScreen;
			renderX = (windowWidth - gameWidth * smallest) / 2;
			renderY = (windowHeight - gameHeight * smallest) / 2;
			renderWidthRatio = smallest;
			renderHeightRatio = smallest;
			break;
		case STRETCH_FILL:
			xScaleScreen = windowWidth / (float) gameWidth;
			yScaleScreen = windowHeight / (float) gameHeight;
			float biggest = xScaleScreen > yScaleScreen ? xScaleScreen : yScaleScreen;
			renderX = (windowWidth - gameWidth * biggest) / 2;
			renderY = (windowHeight - gameHeight * biggest) / 2;
			renderWidthRatio = biggest;
			renderHeightRatio = biggest;
			break;
		case STRETCH_RESIZE:
			renderX = 0;
			renderY = 0;
			renderWidthRatio = 1;
			renderHeightRatio = 1;
			break;
		}
	}

	private View setViewScreenLocation(View v) {
		int sX = 0;
		int sY = 0;
		int sWidth = 0;
		int sHeight = 0;
		switch (stretchMode) {
		case STRETCH_NONE:
			sX = (windowWidth - gameWidth) / 2 + v.getScreenX();
			sY = (windowHeight - gameHeight) / 2 + v.getScreenY();
			sWidth = v.getScreenWidth();
			sHeight = v.getScreenHeight();
			break;
		case STRETCH_FULL:
			float xScale = v.getScreenWidth() / (float) gameWidth;
			float yScale = v.getScreenHeight() / (float) gameHeight;
			float xRatio = windowWidth / (float) gameWidth;
			float yRatio = windowHeight / (float) gameHeight;
			sX = (int) (v.getScreenX() * xRatio);
			sY = (int) (v.getScreenY() * yRatio);
			sWidth = (int) (windowWidth * xScale);
			sHeight = (int) (windowHeight * yScale);
			break;
		case STRETCH_SMART:
			float xScaleScreen = windowWidth / (float) gameWidth;
			float yScaleScreen = windowHeight / (float) gameHeight;
			float smallest = xScaleScreen < yScaleScreen ? xScaleScreen : yScaleScreen;
			sX = (int) ((windowWidth - gameWidth * smallest) / 2 + v.getScreenX() * smallest);
			sY = (int) ((windowHeight - gameHeight * smallest) / 2 + v.getScreenY() * smallest);
			sWidth = (int) Math.ceil(v.getScreenWidth() * smallest);
			sHeight = (int) Math.ceil(v.getScreenHeight() * smallest);
			break;
		case STRETCH_FILL:
			xScaleScreen = windowWidth / (float) gameWidth;
			yScaleScreen = windowHeight / (float) gameHeight;
			float biggest = xScaleScreen > yScaleScreen ? xScaleScreen : yScaleScreen;
			sX = (int) ((windowWidth - gameWidth * biggest) / 2 + v.getScreenX() * biggest);
			sY = (int) ((windowHeight - gameHeight * biggest) / 2 + v.getScreenY() * biggest);
			sWidth = (int) Math.ceil(v.getScreenWidth() * biggest);
			sHeight = (int) Math.ceil(v.getScreenHeight() * biggest);
			break;
		case STRETCH_RESIZE:
			sX = 0;
			sY = 0;
			sWidth = windowWidth;
			sHeight = windowHeight;
			v.setSize(windowWidth, windowHeight);
			break;
		}
		v.setRenderLocation(sX, windowHeight - sY - sHeight, sWidth, sHeight);
		return v;
	}

	/* Utilities */

	public void printString(String message) {
		System.out.println(message);
	}

	public void printWarning(String message) {
		System.out.println("[War] " + message);
	}

	public void printError(String message) {
		System.out.println("[ERR] " + message);
	}

	public void printFatal(String message) {
		System.out.println("[FATAL ERROR] " + message);
		stop();
	}

}
