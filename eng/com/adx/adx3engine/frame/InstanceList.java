package com.adx.adx3engine.frame;

import java.util.List;
import java.util.function.Function;

import com.adx.adx3engine.maths.ListUtils;

/**
 * Instance List utility.
 * This class stocks a list of instances of a given type and gives tools to manipulate and test them.
 * @author Alexandre Desbiens
 * @param <T> Instance type
 */
public abstract class InstanceList<T extends Instance> extends ObjectList<T> {
	
	public InstanceList() {
		super();
	}
	
	/**
	 * Returns the instance list.
	 * @return Instance list
	 */
	public List<T> getInstanceList() {
		return super.getInstanceList();
	}
	
	public int getCount() {
		return super.getInstanceList().size();
	}
	
	/**
	 * Returns the ListUtils linked to the instance list.
	 * @return Linked ListUtils
	 */
	public ListUtils<T> getListUtils() {
		return super.getListUtils();
	}
	
	/**
	 * Tests all instances in list against the given function.
	 * @param f Testing function
	 * @return True if any of the instances matched the given function, false otherwise
	 */
	public boolean test(Function<T, Boolean> f) {
		return super.test(f);
	}
	
	/**
	 * Adds an instance to the instance list.
	 * @param instance Instance to add
	 */
	public void addInstance(Game game, T instance) {
		super.addInstance(instance);
		game.addInstance(instance);
		instance.setInstanceList(this);
	}
	
	/**
	 * Removes an instance at the given index.
	 * @param index Index
	 */
	public T removeInstance(Game game, int index) {
		T instance = super.removeInstance(index);;
		if (instance != null) {
			game.removeInstance(instance);
		}
		return instance;
	}
	
	/**
	 * Removes a given instance from the instance list.
	 * @param instance Instance to remove
	 */
	public void removeInstance(Game game, Instance instance) {
		super.removeInstance(instance);
		game.removeInstance(instance);
	}
	
	public InstanceList<T> clear(Game game) {
		for (Instance instance : super.getInstanceList()) {
			instance.destroy(game);
		}
		super.clear();
		return this;
	}
	
}
