package com.adx.adx3engine.sound;

public abstract class Sound {
	
	public abstract void play();
	public abstract void loop();
	public abstract void pause();
	public abstract void stop();
	public abstract void rewind();
	public abstract void setVolume(float volume);
	
}
