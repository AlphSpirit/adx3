package com.adx.adx3engine.sound;

import org.lwjgl.BufferUtils;
import org.lwjgl.stb.STBVorbisInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.stb.STBVorbis.*;
import static org.lwjgl.system.MemoryUtil.*;

public class VorbisSound extends Sound {
	
	private static HashMap<String, Integer> mapSound = new HashMap<String, Integer>();
	
	//private String resourcePath;
	private ByteBuffer pcm;
	private int buffer;
	private int source;
	
	/*
	 * Mono8 -> al_FORMAT_MONO8
   	   Mono16 -> al_FORMAT_MONO16
       Stereo8 -> al_FORMAT_STEREO8
       Stereo16 -> al_FORMAT_STEREO16
	 */
	public VorbisSound(String path) {
		//resourcePath = path;
		STBVorbisInfo info = new STBVorbisInfo();
		if (mapSound.containsKey(path)) {
			buffer = mapSound.get(path);
		} else {
			System.out.println("Loading Sound " + path + "...");
			pcm = readVorbis(path, info);
			buffer = alGenBuffers();
			alBufferData(buffer, AL_FORMAT_STEREO16, pcm, info.getSampleRate());
			mapSound.put(path, buffer);
		}
		source = alGenSources();
		//alBufferData(buffer, AL_FORMAT_MONO16, pcm, info.getSampleRate()); <- Put this on for crazy effect lel
		alSourcei(source, AL_BUFFER, buffer);
	}

	@Override
	public void play() {
		alSourcei(source, AL_LOOPING, 0);
		alSourcePlay(source);
	}
	
	@Override
	public void loop() {
		alSourcei(source, AL_LOOPING, 1);
		alSourcePlay(source);
	}
	
	@Override
	public void pause() {
		alSourcePause(source);
	}
	
	@Override
	public void stop() {
		alSourceStop(source);
	}
	
	@Override
	public void rewind() {
		alSourceRewind(source);
	}
	
	@Override
	public void setVolume(float volume) {
		alSourcef(source, AL_GAIN, volume);
	}
	
	private ByteBuffer readVorbis(String resourcePath, STBVorbisInfo info) {
		
		ByteBuffer vorbis;
		
		vorbis = ioResourceToByteBuffer(resourcePath);
		
		IntBuffer error = BufferUtils.createIntBuffer(1);
		long decoder = stb_vorbis_open_memory(vorbis, error, null);
		if ( decoder == NULL )
			throw new RuntimeException("Failed to open Ogg Vorbis file. Error: " + error.get(0));

		stb_vorbis_get_info(decoder, info.buffer());

		int channels = info.getChannels();

		int lengthSamples = stb_vorbis_stream_length_in_samples(decoder) * channels;

		ByteBuffer pcm = BufferUtils.createByteBuffer(lengthSamples * 2);

		stb_vorbis_get_samples_short_interleaved(decoder, channels, pcm, lengthSamples);
		stb_vorbis_close(decoder);

		return pcm;
		
	}
	
	/*private static ByteBuffer resizeBuffer(ByteBuffer buffer, int newCapacity) {
		ByteBuffer newBuffer = BufferUtils.createByteBuffer(newCapacity);
		buffer.flip();
		newBuffer.put(buffer);
		return newBuffer;
	}*/

	
	private static ByteBuffer ioResourceToByteBuffer(String resourcePath) {
		ByteBuffer buffer = null;

		File file = new File(resourcePath);
		if ( file.isFile() ) {
			try {
			FileInputStream fis = new FileInputStream(file);
			FileChannel fc = fis.getChannel();
			
			buffer = BufferUtils.createByteBuffer((int)fc.size() + 1);

			while ( fc.read(buffer) != -1 ) ;
			
			fis.close();
			fc.close();
			} catch (IOException e) {
				System.out.println(e.getMessage());
				return buffer;
			}

			buffer.flip();
		}

		return buffer;
	}

}
