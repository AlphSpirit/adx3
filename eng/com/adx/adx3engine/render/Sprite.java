package com.adx.adx3engine.render;

public class Sprite extends Texture {
	
	private float originX;
	private float originY;

	public Sprite(String path, float originX, float originY) {
		super(path);
		this.originX = getWidth() * originX;
		this.originY = getHeight() * originY;
	}

	public float getOriginX() {
		return originX;
	}
	
	public float getOriginY() {
		return originY;
	}

	public void setOrigin(int x, int y) {
		originX = x;
		originY = y;
	}

}
