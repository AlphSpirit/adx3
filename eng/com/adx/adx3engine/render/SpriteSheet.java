package com.adx.adx3engine.render;

public class SpriteSheet extends Texture {
	
	private int imagesX;
	private int imagesY;
	private int imageWidth;
	private int imageHeight;
	private int originX;
	private int originY;
	
	public SpriteSheet(String path, float originX, float originY, int imagesX, int imagesY) {
		super(path);
		this.imagesX = imagesX;
		this.imagesY = imagesY;
		imageWidth = getWidth() / imagesX;
		imageHeight = getHeight() / imagesY;
		this.originX = (int) (imageWidth * originX);
		this.originY = (int) (imageHeight * originY);
	}
	
	public void render(GraphicsRenderer g, float x, float y, int imageX, int imageY) {
		g.drawTextureSizedPartial(this, x - originX, y - originY, imageWidth, imageHeight, imageX / (float) imagesX, imageY / (float) imagesY, (imageX + 1) / (float) imagesX, (imageY + 1) / (float) imagesY);
	}
	
	public void render(GraphicsRenderer g, float x, float y, int imageX, int imageY, Color c) {
		g.drawTextureSizedPartial(this, x - originX, y - originY, imageWidth, imageHeight, imageX / (float) imagesX, imageY / (float) imagesY, (imageX + 1) / (float) imagesX, (imageY + 1) / (float) imagesY, c);
	}

}
