package com.adx.adx3engine.render;

import com.adx.adx3engine.frame.Game;
import com.adx.adx3engine.maths.Transform;

public abstract class GraphicsRenderer {
	
	protected static Texture texWhite = null;

	private int id = -1;
	public Transform transform = new Transform();
	protected Surface renderTarget = null;

	public GraphicsRenderer(int id) {
		this.id = id;
	}
	
	public static void createWhiteTexture(Game game) {
		if (texWhite == null) {
			int[] i = new int[1];
			i[0] = 0xFFFFFFFF;
			texWhite = new Texture(1, 1, i, "WHITE");
		}
	}

	public int getID() {
		return id;
	}

	public abstract void init(View v);
	public abstract void deinit();
	
	public void startDraw() {
		if (renderTarget != null) {
            renderTarget.bind(this, true);
        }
	}
	
	public void endDraw() {
        if (renderTarget != null) {
            renderTarget.unbind(this);
        }
	}
	
	public Surface getRenderTarget() {
		return renderTarget;
	}
	
    public void setRenderTarget(Surface target) {
    	renderTarget = target;
    }

	public abstract void setColor(float r, float g, float b, float a);
	public abstract void setColor(int r, int g, int b, int a);
	public abstract void setColor(Color c);
	
	public abstract void setShader(Shader shader);
	
	public abstract void setBlendingFunction(int sourceIndex, int destinationIndex);
	
	// Outlines
	
	public abstract void outlineRectangle(float x, float y, float width, float height);
	public abstract void outlineRectangle(float x, float y, float width, float height, Color c);
	
	public abstract void outlineCircle(float x, float y, float radius);
	public abstract void outlineCircle(float x, float y, float radius, Color c);
	
	// Filled shapes
	
	public abstract void drawLine(float x1, float y1, float x2, float y2);
	public abstract void drawLine(float x1, float y1, float x2, float y2, Color c);

	public abstract void drawRectangle(float x, float y, float width, float height);
	public abstract void drawRectangle(float x, float y, float width, float height, Color c);
	public abstract void drawRectangleColored(float x, float y, float width, float height, Color c1, Color c2, Color c3, Color c4);
	
	public abstract void drawCircle(float x, float y, float radius);
	public abstract void drawCircle(float x, float y, float radius, Color c);
	
	public abstract void drawPolygon(int type, float[] x, float[] y);
	public abstract void drawPolygon(int type, float[] x, float[] y, Color c);
	
	// Fonts
	
	public abstract void drawText(Font font, String text, float x, float y);
	public abstract void drawText(Font font, String text, float x, float y, Color c);
	
	// Textures and surfaces

	public abstract void drawTexture(Texture texture, float x, float y);
	public abstract void drawTexture(Texture texture, float x, float y, Color c);
	public abstract void drawTextureSized(Texture texture, float x, float y, float width, float height);
	public abstract void drawTextureSized(Texture texture, float x, float y, float width, float height, Color c);
	public abstract void drawTextureSizedPartial(Texture texture, float x, float y, float width, float height, float tx1, float ty1, float tx2, float ty2);
	public abstract void drawTextureSizedPartial(Texture texture, float x, float y, float width, float height, float tx1, float ty1, float tx2, float ty2, Color c);
	
	public abstract void drawSprite(Sprite sprite, float x, float y);
	public abstract void drawSprite(Sprite sprite, float x, float y, Color c);

	public abstract void drawSurface(Surface surface, float x, float y);
	public abstract void drawSurface(Surface surface, float x, float y, Color c);
	public abstract void drawSurfaceSized(Surface surface, float x, float y, float width, float height);
	public abstract void drawSurfaceSized(Surface surface, float x, float y, float width, float height, Color color);

}
