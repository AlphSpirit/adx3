package com.adx.adx3engine.render;

import org.lwjgl.opengl.ARBShaderObjects;

import com.adx.adx3engine.maths.Vector;

import static org.lwjgl.opengl.GL11.*;

public class OpenGLRenderer extends GraphicsRenderer {
	
	public int currentShape = 0;
	public int currentTexture = -1;
	public Shader currentShader = null;
	private Vector v = new Vector(0, 0);
	
	public OpenGLRenderer(int id) {
		super(id);
	}

	@Override
	public void init(View v) {

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glViewport(v.getRenderX(), v.getRenderY(), v.getRenderWidth(), v.getRenderHeight());
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glOrtho(v.getX(), v.getX() + v.getWidth(), v.getY() + v.getHeight(), v.getY(), 1, -1);

	}
	
	@Override
	public void deinit() {
	}
	
	@Override
	public void startDraw() {
		super.startDraw();
		currentTexture = -1;
	}
	
	@Override
	public void endDraw() {
		endShape();
		super.endDraw();
	}

	@Override
	public void setColor(float r, float g, float b, float a) {
		glColor4f(r, g, b, a);
	}

	@Override
	public void setColor(int r, int g, int b, int a) {
		glColor4f(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
	}

	@Override
	public void setColor(Color c) {
		glColor4f(c.r, c.g, c.b, c.a);
	}
	
	public void setShader(Shader shader) {
		if (currentShader == shader) {
			return;
		}
		endShape();
		int program;
		if (shader != null) {
			program = shader.getProgram();
		} else {
			program = 0;
		}
		ARBShaderObjects.glUseProgramObjectARB(program);
		currentShader = shader;
	}
	
	@Override
	public void setBlendingFunction(int sourceIndex, int destinationIndex) {
		endShape();
		glBlendFunc(sourceIndex, destinationIndex);
	}

	// Outlines
	
	@Override
	public void outlineRectangle(float x, float y, float width, float height) {
		
		drawRectangle(x, y, width, 1);
		drawRectangle(x, y, 1, height);
		drawRectangle(x, y + height, width + 1, 1);
		drawRectangle(x + width, y, 1, height + 1);
		
	}
	
	@Override
	public void outlineRectangle(float x, float y, float width, float height, Color c) {
		setColor(c);
		outlineRectangle(x, y, width, height);
	}
	
	@Override
	public void outlineCircle(float x, float y, float radius) {

		int precision = 32;
		float angle1 = 0;
		float angle2 = 0;
		for (int i = 0; i < precision + 1; i++) {
			angle2 = (float) Math.PI * 2 * ((i + 1) / (float) precision);
			drawLine((float) Math.cos(angle1) * radius + x, (float) Math.sin(angle1) * radius + y, (float) Math.cos(angle2) * radius + x, (float) Math.sin(angle2) * radius + y);
			angle1 = angle2;
		}
		
	}
	
	@Override
	public void outlineCircle(float x, float y, float radius, Color c) {
		setColor(c);
		outlineCircle(x, y, radius);
	}
	
	// Filled shapes
	
	@Override
	public void drawLine(float x1, float y1, float x2, float y2) {
		
		checkShapeTexture(GL_LINES, 0);
		if (y2 > y1) {
			y2 += 1;
		}
		vertex(x1, y1);
		vertex(x2, y2);
		
	}
	
	@Override
	public void drawLine(float x1, float y1, float x2, float y2, Color c) {
		setColor(c);
		drawLine(x1, y1, x2, y2);
	}
	
	@Override
	public void drawRectangle(float x, float y, float width, float height) {

		checkShapeTexture(GL_QUADS, 0);
		vertex(x, y);
		vertex(x + width, y);
		vertex(x + width, y + height);
		vertex(x, y + height);

	}
	
	@Override
	public void drawRectangle(float x, float y, float width, float height, Color c) {
		setColor(c);
		drawRectangle(x, y, width, height);
	}
	
	@Override
	public void drawRectangleColored(float x, float y, float width, float height, Color c1, Color c2, Color c3, Color c4) {
		
		checkShapeTexture(GL_QUADS, 0);
		setColor(c1);
		glVertex2f(x, y);
		setColor(c2);
		glVertex2f(x + width, y);
		setColor(c3);
		glVertex2f(x + width, y + height);
		setColor(c4);
		glVertex2f(x, y + height);
		
	}
	
	@Override
	public void drawCircle(float x, float y, float radius) {
		
		int precision = 32;
		checkShapeTexture(GL_TRIANGLE_FAN, 0);
		vertex(x, y);
		float angle;
		for (int i = 0; i < precision + 1; i++) {
			angle = (float) Math.PI * 2 * (i / (float) precision);
			vertex(x + radius * (float) Math.cos(angle), y + radius * (float) Math.sin(angle));
		}
		endShape();
		
		removePolygonErrorsOnIntelCards();
		
	}
	
	@Override
	public void drawCircle(float x, float y, float radius, Color c) {
		setColor(c);
		drawCircle(x, y, radius);
	}
	
	@Override
	public void drawPolygon(int type, float[] x, float[] y) {
		
		if (x.length != y.length) {
			return;
		}
		checkShapeTexture(type, new Texture("WHITE").getTextureID());
		for (int i = 0; i < x.length; i++) {
			vertex(x[i], y[i]);
		}
		endShape();
		
		removePolygonErrorsOnIntelCards();
		
	}
	
	@Override
	public void drawPolygon(int type, float[] x, float[] y, Color c) {
		setColor(c);
		drawPolygon(type, x, y);
	}
	
	/* Fonts */
	
	public void drawText(Font font, String text, float x, float y) {
		font.draw(this, text, x, y);
	}
	
	@Override
	public void drawText(Font font, String text, float x, float y, Color c) {
		setColor(c);
		drawText(font, text, x, y);
	}
	
	/* Textures and surfaces */

	@Override
	public void drawTexture(Texture texture, float x, float y) {

		checkShapeTexture(GL_QUADS, texture.getTextureID());
		glTexCoord2f(0, 0);
		vertex(x, y);
		glTexCoord2f(1, 0);
		vertex(x + texture.getWidth(), y);
		glTexCoord2f(1, 1);
		vertex(x + texture.getWidth(), y + texture.getHeight());
		glTexCoord2f(0, 1);
		vertex(x, y + texture.getHeight());

	}
	
	@Override
	public void drawTexture(Texture texture, float x, float y, Color c) {
		setColor(c);
		drawTexture(texture, x, y);
	}
	
	@Override
	public void drawTextureSized(Texture texture, float x, float y, float width, float height) {
		
		checkShapeTexture(GL_QUADS, texture.getTextureID());
		glTexCoord2f(0, 0);
		vertex(x, y);
		glTexCoord2f(1, 0);
		vertex(x + width, y);
		glTexCoord2f(1, 1);
		vertex(x + width, y + height);
		glTexCoord2f(0, 1);
		vertex(x, y + height);
		
	}
	
	@Override
	public void drawTextureSized(Texture texture, float x, float y, float width, float height, Color c) {
		setColor(c);
		drawTextureSized(texture, x, y, width, height);
	}

	@Override
	public void drawTextureSizedPartial(Texture texture, float x, float y, float width, float height, float tx1, float ty1, float tx2, float ty2) {

		checkShapeTexture(GL_QUADS, texture.getTextureID());
		glTexCoord2f(tx1, ty1);
		vertex(x, y);
		glTexCoord2f(tx2, ty1);
		vertex(x + width, y);
		glTexCoord2f(tx2, ty2);
		vertex(x + width, y + height);
		glTexCoord2f(tx1, ty2);
		vertex(x, y + height);

	}
	
	@Override
	public void drawTextureSizedPartial(Texture texture, float x, float y, float width, float height, float tx1, float ty1, float tx2, float ty2, Color c) {
		setColor(c);
		drawTextureSizedPartial(texture, x, y, width, height, tx1, ty1, tx2, ty2);
	}
	
	@Override
	public void drawSprite(Sprite sprite, float x, float y) {
		
		checkShapeTexture(GL_QUADS, sprite.getTextureID());
		glTexCoord2f(0, 0);
		vertex(x - sprite.getOriginX(), y - sprite.getOriginY());
		glTexCoord2f(1, 0);
		vertex(x + sprite.getWidth() - sprite.getOriginX(), y - sprite.getOriginY());
		glTexCoord2f(1, 1);
		vertex(x + sprite.getWidth() - sprite.getOriginX(), y + sprite.getHeight() - sprite.getOriginY());
		glTexCoord2f(0, 1);
		vertex(x - sprite.getOriginX(), y + sprite.getHeight() - sprite.getOriginY());
		
	}
	
	@Override
	public void drawSprite(Sprite sprite, float x, float y, Color c) {
		setColor(c);
		drawSprite(sprite, x, y);
	}

	@Override
	public void drawSurface(Surface surface, float x, float y) {

		checkShapeTexture(GL_QUADS, surface.getTextureID());
		glTexCoord2f(0, 0);
		glVertex2f(x, y + surface.getHeight());
		glTexCoord2f(1, 0);
		glVertex2f(x + surface.getWidth(), y + surface.getHeight());
		glTexCoord2f(1, 1);
		glVertex2f(x + surface.getWidth(), y);
		glTexCoord2f(0, 1);
		glVertex2f(x, y);
		
	}
	
	@Override
	public void drawSurface(Surface surface, float x, float y, Color c) {
		setColor(c);
		drawSurface(surface, x, y);
	}
	
	@Override
	public void drawSurfaceSized(Surface surface, float x, float y, float width, float height) {
		checkShapeTexture(GL_QUADS, surface.getTextureID());
		glTexCoord2f(0, 0);
		glVertex2f(x, y + height);
		glTexCoord2f(1, 0);
		glVertex2f(x + width, y + height);
		glTexCoord2f(1, 1);
		glVertex2f(x + width, y);
		glTexCoord2f(0, 1);
		glVertex2f(x, y);
	}
	
	@Override
	public void drawSurfaceSized(Surface surface, float x, float y, float width, float height, Color color) {
		setColor(color);
		drawSurfaceSized(surface, x, y, width, height);
	}
	
	/* Utilities */
	
	public void checkShapeTexture(int shape, int texture) {
		if (currentShape == shape && currentTexture == texture) {
			return;
		}
		glEnd();
		if (currentTexture != texture) {
			currentTexture = texture;
			glBindTexture(GL_TEXTURE_2D, texture);
		}
		glBegin(shape);
		currentShape = shape;
	}
	
	public void endShape() {
		if (currentShape != 0) {
			glEnd();
			currentShape = 0;
		}
	}
	
	public void vertex(float x, float y) {
		v.setPosition(x, y);
		transform.applyTransformations(v);
		glVertex2f(v.x, v.y);
	}
	
	public int removePolygonErrorsOnIntelCards() {
		return glGetError();
	}

}
