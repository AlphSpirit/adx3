package com.adx.adx3engine.render;

public class Color {
	
	// Small collection of basic colors
	public static final Color WHITE = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	public static final Color BLACK = new Color(0.0f, 0.0f, 0.0f, 1.0f);
	public static final Color RED = new Color(1.0f, 0.0f, 0.0f, 1.0f);
	public static final Color GREEN = new Color(0.0f, 1.0f, 0.0f, 1.0f);
	public static final Color BLUE = new Color(0.0f, 0.0f, 1.0f, 1.0f);
	public static final Color YELLOW = new Color(1.0f, 1.0f, 0.0f, 1.0f);
	public static final Color ORANGE = new Color(1.0f, 0.5f, 0.0f, 1.0f);
	public static final Color MAGENTA = new Color(1.0f, 0.0f, 1.0f, 1.0f);
	public static final Color CYAN = new Color(0.0f, 1.0f, 1.0f, 1.0f);
	public static final Color GRAY = new Color(0.5f, 0.5f, 0.5f, 1.0f);
	public static final Color TRANSPARENT = new Color(0.0f, 0.0f, 0.0f, 0.0f);
	
	public float r;
	public float g;
	public float b;
	public float a;
	
	public Color(float r, float g, float b) {
		this(r, g, b, 1.0f);
	}
	
	public Color(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public Color(int r, int g, int b) {
		this(r / 255.0f, g / 255.0f, b / 255.0f, 1.0f);
	}
	
	public Color(int r, int g, int b, int a) {
		this(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
	}
	
	public Color darken(float intensity) {
		float mul = 1.0f - intensity;
		return new Color(r * mul, g * mul, b * mul, a);
	}
	
	public Color lighten(float intensity) {
		float mul = 1.0f + intensity;
		return new Color(r * mul, g * mul, b * mul, a);
	}
	
	@Override
	public String toString() {
		return "Color: (" + r + ", " + g + ", " + b + ")";
	}

	public Color copy() {
		return new Color(r, g, b, a);
	}

}
