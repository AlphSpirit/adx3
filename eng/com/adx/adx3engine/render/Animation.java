package com.adx.adx3engine.render;

import java.util.HashMap;

public class Animation {
	
	private SpriteSheet spriteSheet;
	private HashMap<String, AnimationString> animationStrings = new HashMap<String, AnimationString>();
	private String currentAnimationName = null;
	private AnimationString currentAnimation = null;
	private int animationImageX = 0;
	private int animationImageY = 0;
	private int animationIndex = 0;
	private int animationTimer = 0;
	
	public Animation(SpriteSheet spriteSheet) {
		this.spriteSheet = spriteSheet;
	}
	
	public static class AnimationString {
		
		private KeyFrame[] keyFrames;
		
		public AnimationString(KeyFrame... keyFrames) {
			this.keyFrames = keyFrames;
		}
		
		public int getLength() {
			return keyFrames.length;
		}
		
		public KeyFrame getKeyFrame(int index) {
			return keyFrames[index];
		}
		
	}
	
	public static class KeyFrame {
		
		private int imageX;
		private int imageY;
		private int imageTime;
		
		public KeyFrame(int imageX, int imageY, int imageTime) {
			this.imageX = imageX;
			this.imageY = imageY;
			this.imageTime = imageTime;
		}
		
	}
	
	public void addAnimationString(String name, AnimationString string) {
		animationStrings.put(name, string);
	}
	
	public void startAnimation(String name) {
		if (currentAnimationName != null && currentAnimationName.equals(name)) {
			return;
		}
		AnimationString string = animationStrings.get(name);
		if (string != null) {
			currentAnimation = string;
			currentAnimationName = name;
			animationIndex = 0;
			animationImageX = string.getKeyFrame(0).imageX;
			animationImageY = string.getKeyFrame(0).imageY;
		}
	}
	
	public void render(GraphicsRenderer g, float x, float y) {
		spriteSheet.render(g, x, y, animationImageX, animationImageY);
		if (currentAnimation != null) {
			animationTimer++;
			if (animationTimer >= currentAnimation.getKeyFrame(animationIndex).imageTime) {
				animationTimer = 0;
				animationIndex++;
				if (animationIndex >= currentAnimation.getLength()) {
					animationIndex = 0;
				}
				KeyFrame keyFrame = currentAnimation.getKeyFrame(animationIndex);
				animationImageX = keyFrame.imageX;
				animationImageY = keyFrame.imageY;
			}
		}
	}
	
	public void render(GraphicsRenderer g, float x, float y, Color c) {
		g.setColor(c);
		render(g, x, y);
	}

}
