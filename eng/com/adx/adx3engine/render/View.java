package com.adx.adx3engine.render;

import com.adx.adx3engine.shapes.CRectangle;

public class View {
	
	private int id;
	private CRectangle inGame;
	private CRectangle onScreen;
	private CRectangle toRender;
	
	public View(int id, int x, int y, int width, int height, int screenX, int screenY, int screenWidth, int screenHeight) {
		this.id = id;
		inGame = new CRectangle(x, y, width, height);
		onScreen = new CRectangle(screenX, screenY, screenWidth, screenHeight);
		toRender = new CRectangle(0, 0, 0, 0);
	}
	
	public int getID() {
		return id;
	}
	
	public int getX() {
		return (int) inGame.x;
	}
	
	public void setX(int x) {
		inGame.x = x;
	}
	
	public int getY() {
		return (int) inGame.y;
	}
	
	public void setY(int y) {
		inGame.y = y;
	}
	
	public void setPosition(int x, int y) {
		inGame.x = x;
		inGame.y = y;
	}
	
	public void setCenterPosition(int x, int y) {
		inGame.x = x - inGame.width / 2;
		inGame.y = y - inGame.height / 2;
	}
	
	public void addPosition(int x, int y) {
		inGame.x += x;
		inGame.y += y;
	}
	
	public int getWidth() {
		return (int) inGame.width;
	}
	
	public int getHeight() {
		return (int) inGame.height;
	}
	
	public void setSize(int width, int height) {
		inGame.width = width;
		inGame.height = height;
	}
	
	public CRectangle getRectangle() {
		return inGame;
	}
	
	/* On screen rectangle */
	
	public int getScreenX() {
		return (int) onScreen.x;
	}
	
	public int getScreenY() {
		return (int) onScreen.y;
	}
	
	public int getScreenWidth() {
		return (int) onScreen.width;
	}
	
	public int getScreenHeight() {
		return (int) onScreen.height;
	}
	
	/* Render rectangle */
	
	public int getRenderX() {
		return (int) toRender.x;
	}
	
	public int getRenderY() {
		return (int) toRender.y;
	}
	
	public int getRenderWidth() {
		return (int) toRender.width;
	}
	
	public int getRenderHeight() {
		return (int) toRender.height;
	}
	
	public void setRenderLocation(int x, int y, int width, int height) {
		toRender.x = x;
		toRender.y = y;
		toRender.width = width;
		toRender.height = height;
	}

}
