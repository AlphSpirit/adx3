package com.adx.adx3engine.maths;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.adx.adx3engine.maths.astar.AStarImplementation;
import com.adx.adx3engine.maths.astar.AreaMap;

public class AStar {
	
	private int width;
	private int height;
	private int cellWidth;
	private int cellHeight;
	private AStarImplementation aStarImplementation;
	
	public AStar(int[][] map, int width, int height) {
		this(map, width, height, 1, 1);
	}
	
	public AStar(int[][] map, int width, int height, int cellWidth, int cellHeight) {
		aStarImplementation = new AStarImplementation(new AreaMap(width, height, map));
		this.width = width;
		this.height = height;
		this.cellWidth = cellWidth;
		this.cellHeight = cellHeight;
	}
	
	public List<Point> getPath(int startX, int startY, int endX, int endY) {
		if (startX < 0 || startY < 0 || startX >= width || startY > height
				|| endX < 0 || endY < 0 || endX >= width || endY > height) {
			return new ArrayList<Point>();
		}
		List<Point> list = aStarImplementation.calcShortestPath(startX, startY, endX, endY);
		if (list == null) {
			return new ArrayList<Point>();
		}
		return list;
	}
	
	public List<Point> getPathTruePosition(float startX, float startY, float endX, float endY) {
		return getPath((int) startX / cellWidth, (int) startY / cellHeight, (int) endX / cellWidth, (int) endY / cellHeight);
	}

}
