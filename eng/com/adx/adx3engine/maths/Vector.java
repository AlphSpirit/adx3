package com.adx.adx3engine.maths;

public class Vector {
	
	public float x;
	public float y;
	
	public Vector(float x, float y) {
		setPosition(x, y);
	}
	
	public static float dot(Vector v1, Vector v2) {
		return v1.getLength() * v2.getLength() * (v2.getAngle() - v1.getAngle());
	}
	
	public float getLength() {
		return MathUtils.dist(x, y);
	}
	
	public float getAngle() {
		return MathUtils.angle(x, y);
	}
	
	public Vector add(Vector v) {
		return new Vector(x + v.x, y + v.y);
	}
	
	public Vector sub(Vector v) {
		return new Vector(x - v.x, y - v.y);
	}
	
	public Vector mul(float s) {
		return new Vector(x * s, y * s);
	}
	
	public void setPosition(float x, float y) {
		this.x = x;
		this.y = y;
	}

}
