package com.adx.adx3engine.maths;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * List Utilities class.
 * This class contains utilities functions that can navigate, sum or test lists.
 * @author Alexandre Desbiens
 * @param <T> Type of list that will be passed to the ListUtils
 */
public class ListUtils<T> {
	
	/**
	 * Sums all the values of the given generic list from left to right.
	 * @param l Generic list to fold
	 * @return The sum of all the elements of the list
	 */
	public T sum(List<T> l) {
		T t = l.get(0);
		for (int i = 1; i < l.size(); i++) {
			t = add(t, l.get(i));
		}
		return t;
	}
	
	/**
	 * Maps a function to all the elements of a generic list and returns a list containing all the results.
	 * @param l Generic list to map
	 * @param f Function with one T parameter that returns T
	 * @return A list containing all the mapped values
	 */
	public List<T> map(List<T> l, Function<T, T> f) {
		List<T> lR = new ArrayList<T>();
		for (T t : l) {
			lR.add(f.apply(t));
		}
		return lR;
	}
	
	/**
	 * Tests all elements against a function and return true of any of the element matched.
	 * @param l Generic list to test
	 * @param f A function with one parameter T that returns true
	 * @return True if any of the elements returns true, false otherwise
	 */
	public boolean test(List<T> l, Function<T, Boolean> f) {
		for (T t : l) {
			if (f.apply(t)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Prints the content of a generic list.
	 * @param l Generic list to print
	 */
	public void printList(List<T> l) {
		for (T t : l) {
			System.out.println(t.toString());
		}
	}
	
	/**
	 * Adds two generic types if possible.
	 * @param t1 Generic value 1
	 * @param t2 Generic value 2
	 * @return The addition of the two generics, or null if the object cannot be added
	 */
	@SuppressWarnings("unchecked")
	private T add(T t1, T t2) {
		if (t1 instanceof Byte) {
			return (T) Byte.valueOf((byte) ((Byte) t1 + (Byte) t2));
		}
		if (t1 instanceof Short) {
			return (T) Short.valueOf((short) ((Short) t1 + (Short) t2));
		}
		if (t1 instanceof Integer) {
			return (T) Integer.valueOf((Integer) t1 + (Integer) t2);
		}
		if (t1 instanceof Long) {
			return (T) Long.valueOf((Long) t1 + (Long) t2);
		}
		if (t1 instanceof Float) {
			return (T) Float.valueOf((Float) t1 + (Float) t2);
		}
		if (t1 instanceof Double) {
			return (T) Double.valueOf((Double) t1 + (Double) t2);
		}
		if (t1 instanceof String) {
			return (T) ((String) t1 + (String) t2);
		}
		return null;
	}

}
