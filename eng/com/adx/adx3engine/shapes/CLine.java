package com.adx.adx3engine.shapes;

import com.adx.adx3engine.maths.MathUtils;

/**
 * Simple line structure made of 2 sets of coordinates.
 * @author Alexandre Desbiens
 */
public class CLine extends CShape {
	
	public float x1;
	public float y1;
	public float x2;
	public float y2;
	
	/**
	 * Initializes the structure with 2 sets of coordinates.
	 * @param x1 X1 position
	 * @param y1 Y1 position
	 * @param x2 X2 position
	 * @param y2 Y2 position
	 */
	public CLine(float x1, float y1, float x2, float y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	public float getLenght() {
		return MathUtils.dist(x1, y1, x2, y2);
	}
	
	public float getAngle() {
		return MathUtils.angle(x1, y1, x2, y2);
	}
	
	public void setPosition(float x1, float y1, float x2, float y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	/**
	 * Tests a collision with another line.
	 * @param l Line to collide with
	 * @return Collision point
	 */
	public boolean collides(CLine l) {
		return Collision.getCollision(this, l);
	}

	public CPoint getCollisionPoint(CLine line) {
		return Collision.getCollisionPoint(this, line);
	}

}
