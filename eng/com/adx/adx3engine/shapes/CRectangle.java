package com.adx.adx3engine.shapes;

public class CRectangle extends CShape {
	
	public float x;
	public float y;
	public float width;
	public float height;
	
	public CRectangle() {
		x = 0;
		y = 0;
		width = 0;
		height = 0;
	}
	
	public CRectangle(float x, float y, float width, float height) {
		
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
	}
	
	/**
	 * Initializes the structure by creating the smallest rectangle that contains the two given points.
	 * @param p1 First point to contain
	 * @param p2 Second point to contain
	 */
	public CRectangle(CPoint p1, CPoint p2) {
		if (p1.x < p2.x) {
			x = p1.x;
		} else {
			x = p2.x;
		}
		if (p1.y < p2.y) {
			y = p1.y;
		} else {
			y = p2.y;
		}
		width = Math.abs(p1.x - p2.x);
		if (width < 1) {
			width = 1;
		}
		height = Math.abs(p1.y - p2.y);
		if (height < 1) {
			height = 1;
		}
	}
	
	public void reset(float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void setPosition(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public boolean collides(CPoint p ) {
		return Collision.getCollision(p, this);
	}
	
	public boolean collides(CRectangle r) {
		return Collision.getCollision(this, r);
	}
	
	public boolean collides(CCircle c) {
		return Collision.getCollision(this, c);
	}

}
