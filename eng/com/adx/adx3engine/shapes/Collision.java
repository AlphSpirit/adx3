package com.adx.adx3engine.shapes;

import java.util.ArrayList;
import java.util.List;

import com.adx.adx3engine.maths.MathUtils;
import com.adx.adx3engine.maths.Vector;

public abstract class Collision {

    public static boolean getCollision(CPoint p, CRectangle r) {
        return p.x >= r.x && p.x < r.x + r.width && p.y >= r.y && p.y < r.y + r.height;
    }
	
	public static boolean getCollision(CRectangle r1, CRectangle r2) {
		return r1.x + r1.width > r2.x && r1.y + r1.height > r2.y && r1.x < r2.x + r2.width && r1.y < r2.y + r2.height;
	}
	
	public static boolean getCollision(CRectangle r, CCircle c) {
		float x = MathUtils.clamp(c.x, r.x, r.x + r.width);
		float y = MathUtils.clamp(c.y, r.y, r.y + r.height);
		return MathUtils.dist(x, y, c.x, c.y) <= c.radius;
	}
	
	/**
	 * Retrieves the collision point between 2 lines.
	 * @param l1 Line 1 to collide
	 * @param l2 Line 2 to collide
	 * @return Collision point, or null if there is no collision
	 */
	public static CPoint getCollisionPoint(CLine l1, CLine l2) {
		
		// Start by testing the two rectangles around the lines for optimization
		CRectangle r1 = new CRectangle(new CPoint(l1.x1, l1.y1), new CPoint(l1.x2, l1.y2));
		CRectangle r2 = new CRectangle(new CPoint(l2.x1, l2.y1), new CPoint(l2.x2, l2.y2));
		if (!getCollision(r1, r2)) {
			return null;
		}
		
		// Calculates the repeating bits
		float a = l1.x1 * l1.y2 - l1.y1 * l1.x2;
		float b = l2.x1 * l2.y2 - l2.y1 * l2.x2;
		float c = (l1.x1 - l1.x2) * (l2.y1 - l2.y2) - (l1.y1 - l1.y2) * (l2.x1 - l2.x2);
		
		// If the denominator is 0, the lines are parallel. This case is not
		// handled.
		if (c == 0) {
			return null;
		}
		
		// Find the collision point between the lines
		float x = (a * (l2.x1 - l2.x2) - (l1.x1 - l1.x2) * b) / c;
		float y = (a * (l2.y1 - l2.y2) - (l1.y1 - l1.y2) * b) / c;
		CPoint p = new CPoint(Math.round(x * 100) / 100.0f, Math.round(y * 100) / 100.0f);
		
		// Verify that the point lies within the segments
		if (getCollision(p, r1) && getCollision(p, r2)) {
			return p;
		} else {
			return null;
		}
		
	}
	
	public static boolean getCollision(CLine l, CCircle c) {
		return getClosestPoint(l, c.getCenter()).getDistance(c.getCenter()) <= c.radius;
	}
	
	// http://mathworld.wolfram.com/Circle-LineIntersection.html
	public static CPoint[] getCollisionPoints(CLine l, CCircle c) {

		l = new CLine(l.x1 - c.x, l.y1 - c.y, l.x2 - c.x, l.y2 - c.y);
		float dx = l.x2 - l.x1;
		float dy = l.y2 - l.y1;
		float dr = (float) Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
		float D = l.x1 * l.y2 - l.x2 * l.y1;
		
		float signDy = Math.signum(dy);
		if (signDy == 0) {
			signDy = 1;
		}
		
		float x1 = (float) ((D * dy + signDy * dx * Math.sqrt(Math.pow(c.radius, 2) * Math.pow(dr, 2) - Math.pow(D, 2))) / Math.pow(dr, 2));
		float y1 = (float) ((-D * dx + Math.abs(dy) * Math.sqrt(Math.pow(c.radius, 2) * Math.pow(dr, 2) - Math.pow(D, 2))) / Math.pow(dr, 2));
		
		float x2 = (float) ((D * dy - signDy * dx * Math.sqrt(Math.pow(c.radius, 2) * Math.pow(dr, 2) - Math.pow(D, 2))) / Math.pow(dr, 2));
		float y2 = (float) ((-D * dx - Math.abs(dy) * Math.sqrt(Math.pow(c.radius, 2) * Math.pow(dr, 2) - Math.pow(D, 2))) / Math.pow(dr, 2));
		
		CRectangle r = new CRectangle(new CPoint(l.x1 + c.x, l.y1 + c.y), new CPoint(l.x2 + c.x, l.y2 + c.y));
		List<CPoint> lP = new ArrayList<CPoint>();
		CPoint p1 = new CPoint(x1 + c.x, y1 + c.y);
		CPoint p2 = new CPoint(x2 + c.x, y2 + c.y);
		
		if (p1.collides(r)) {
			lP.add(p1);
		}
		if (p2.collides(r)) {
			lP.add(p2);
		}
		
		return lP.toArray(new CPoint[0]);

	}
	
	// http://stackoverflow.com/questions/3120357/get-closest-point-to-a-line
	public static CPoint getClosestPoint(CLine l, CPoint point) {
		
		Vector a = new Vector(l.x1, l.y1);
		Vector b = new Vector(l.x2, l.y2);
		Vector p = new Vector(point.x, point.y);
		Vector ap = p.sub(a);
		Vector ab = b.sub(a);
		float magnitudeAB = (float) (Math.pow(ab.x, 2) + Math.pow(ab.y, 2));
		float ABAPproduct = ab.x * ap.x + ab.y * ap.y;
		float distance = ABAPproduct / magnitudeAB;
		if (distance < 0) {
			return new CPoint(a.x, a.y);
		}
		if (distance > 1) {
			return new CPoint(b.x, b.y);
		}
		Vector v = a.add(ab.mul(distance));
		return new CPoint(v.x, v.y);
		
	}
	
	public static boolean getCollision(CCircle c1, CCircle c2) {
		return MathUtils.dist(c1.x, c1.y, c2.x, c2.y) <= c1.radius + c2.radius;
	}
	
	public static boolean getCollision(CShape s1, CShape s2) {
		
		if (s1 instanceof CPoint) {
			if (s2 instanceof CPoint) {
				return getCollision((CPoint) s1, (CPoint) s2);
			} else if (s2 instanceof CRectangle) {
				return getCollision((CPoint) s1, (CRectangle) s2);
			} else if (s2 instanceof CCircle) {
				//return getCollision((CPoint) s1, (CCircle) s2);
			}
		} else if (s1 instanceof CRectangle) {
			if (s2 instanceof CPoint) {
				return getCollision((CPoint) s2, (CRectangle) s1);
			} else if (s2 instanceof CRectangle) {
				return getCollision((CRectangle) s1, (CRectangle) s2);
			} else if (s2 instanceof CCircle) {
				return getCollision((CRectangle) s1, (CCircle) s2);
			}
		} else if (s1 instanceof CCircle) {
			if (s2 instanceof CPoint) {
				//return getCollision((CPoint) s2, (CCircle) s1);
			} else if (s2 instanceof CRectangle) {
				return getCollision((CRectangle) s2, (CCircle) s1);
			} else if (s2 instanceof CCircle) {
				return getCollision((CCircle) s1, (CCircle) s2);
			}
		}
		
		return false;
		
	}

}
