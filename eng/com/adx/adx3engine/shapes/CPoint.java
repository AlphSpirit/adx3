package com.adx.adx3engine.shapes;

import com.adx.adx3engine.maths.MathUtils;

public class CPoint extends CShape {

	public float x;
	public float y;

	public CPoint(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public float getDistance(CPoint p) {
		return MathUtils.dist(x, y, p.x, p.y);
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean collides(CRectangle r) {
		return Collision.getCollision(this, r);
	}
	
	public CPoint copy() {
		return new CPoint(x, y);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof CPoint)) {
			return false;
		}
		CPoint p = (CPoint) obj;
		return p.x == x && p.y == y;
	}

}
