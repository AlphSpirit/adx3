package com.adx.adx3engine.shapes;

public abstract class CShape {
	
	public boolean collides(CShape shape) {
		
		return Collision.getCollision(this, shape);
		
	}

}
