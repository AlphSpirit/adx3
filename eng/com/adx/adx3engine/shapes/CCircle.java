package com.adx.adx3engine.shapes;

public class CCircle extends CShape {
	
	public float x;
	public float y;
	public float radius;
	
	public CCircle(float x, float y, float radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}
	
	public CPoint getCenter() {
		return new CPoint(x, y);
	}
	
	public void setPosition(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean collides(CRectangle r) {
		return Collision.getCollision(r, this);
	}
	
	public boolean collides(CCircle c) {
		return Collision.getCollision(this, c);
	}
	
	public CPoint[] getCollisionPoints(CLine line) {
		return Collision.getCollisionPoints(line, this);
	}

}
