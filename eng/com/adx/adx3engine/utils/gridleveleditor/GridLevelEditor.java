package com.adx.adx3engine.utils.gridleveleditor;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.adx.adx3engine.frame.Game;
import com.adx.adx3engine.frame.Room;
import com.adx.adx3engine.io.Button;
import com.adx.adx3engine.io.InputManager;
import com.adx.adx3engine.io.Key;
import com.adx.adx3engine.io.TextFile;
import com.adx.adx3engine.render.Color;
import com.adx.adx3engine.render.GraphicsRenderer;
import com.adx.adx3engine.render.View;
import com.adx.adx3engine.shapes.CPoint;
import com.adx.adx3engine.shapes.CRectangle;

public class GridLevelEditor extends Room {
	
	private static final int MENU_HEIGHT = 64;
	private static final int PANEL_OBJECTS_WIDTH = 256;
	private static final int PANEL_LAYERS_WIDTH = 256;
	
	private static final int MODE_PLACE = 1;
	private static final int MODE_SELECT = 2;
	
	private static final int OPERATION_ADD_OBJECTS = 1;
	private static final int OPERATION_REMOVE_OBJECTS = 2;
	
	// Room informations
	private int roomWidth = 2048;
	private int roomHeight = 1536;
	private int cellWidth = 32;
	private int cellHeight = 32;
	private int gridWidth = roomWidth / cellWidth;
	private int gridHeight = roomHeight / cellHeight;
	
	// View management
	private View view;
	private CPoint previousCell = new CPoint(0, 0);
	private CPoint currentCell = new CPoint(0, 0);
	private boolean mouseLeftIsDown = false;
	
	// Panels
	private boolean panelObjectsOpened = true;
	private boolean panelLayersOpened = true;
	
	// Lists
	private List<PlacableObject> placableObjects = new ArrayList<PlacableObject>();
	private PlacableObject currentPlacableObject = null;
	private List<PlacedObject> placedObjects = new ArrayList<PlacedObject>();
	private int operationIndex = -1;
	private List<Operation> operations = new ArrayList<Operation>();
	private Operation currentOperation = null;
	
	// States
	private int currentMode = MODE_PLACE;
	
	private String saveFile = null;
	private FileNameExtensionFilter filter = new FileNameExtensionFilter("BUFFER map", "bmap");
	private int roomExitTo;
	
	public GridLevelEditor(int roomID, int roomExitTo) {
		super(roomID);
		this.roomExitTo = roomExitTo;
	}
	
	private class PlacableObject {
		
		private int id;
		
		public PlacableObject(int id) {
			this.id = id;
		}
		
	}
	
	private class PlacedObject {
		
		private PlacableObject placableObject;
		private int x;
		private int y;
		private int width;
		private int height;
		
		public PlacedObject(PlacableObject placableObject, int x, int y, int width, int height) {
			this.placableObject = placableObject;
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}
		
		public int getID() {
			return placableObject.id;
		}
		
	}
	
	private class Operation {
		private int type;
		private List<PlacedObject> targetObjects;
		public Operation(int type) {
			this.type = type;
			targetObjects = new ArrayList<PlacedObject>();
		}
	}

	@Override
	public void load(Game game) {
		view = game.getView(Game.VIEW_DEFAULT_ID);
		PlacableObject obj = new PlacableObject(1);
		placableObjects.add(obj);
		currentPlacableObject = obj;
	}

	@Override
	public void update(Game game, InputManager input) {
		
		if (input.getKeyPressed(Key.ESCAPE)) {
			game.changeRoom(roomExitTo);
		}
		
		// Camera movement
		int cameraSpeed = 8;
		if (!input.getKey(Key.LEFT_CONTROL)) {
			if (input.getKey(Key.D)) {
				view.addPosition(cameraSpeed, 0);
			}
			if (input.getKey(Key.A)) {
				view.addPosition(-cameraSpeed, 0);
			}
			if (input.getKey(Key.S)) {
				view.addPosition(0, cameraSpeed);
			}
			if (input.getKey(Key.W)) {
				view.addPosition(0, -cameraSpeed);
			}
		}
		
		// Current cell
		int currentMousePositionX = input.getMouseX() + view.getX();
		if (currentMousePositionX >= 0) {
			currentCell.x = currentMousePositionX / cellWidth;
		} else {
			currentCell.x = currentMousePositionX / cellWidth - 1;
		}
		int currentMousePositionY = input.getMouseY() + view.getY();
		if (currentMousePositionY >= 0) {
			currentCell.y = currentMousePositionY / cellHeight;
		} else {
			currentCell.y = currentMousePositionY / cellHeight - 1;
		}
		if (input.getButtonPressed(Button.LEFT)) {
			previousCell = currentCell.copy();
		}
		mouseLeftIsDown = input.getButton(Button.LEFT);
		
		if (input.getKeyPressed(Key.D1)) {
			currentMode = MODE_PLACE;
		}
		if (input.getKeyPressed(Key.D2)) {
			currentMode = MODE_SELECT;
		}
		
		// Place object
		if (currentMode == MODE_PLACE) {
			if (input.getButtonPressed(Button.LEFT)) {
				newOperation(OPERATION_ADD_OBJECTS);
			} else if (input.getButtonPressed(Button.RIGHT)) {
				newOperation(OPERATION_REMOVE_OBJECTS);
			}
			if (input.getButton(Button.LEFT)) {
				if (getCellsFree((int) currentCell.x, (int) currentCell.y, 1, 1)) {
					PlacedObject obj = new PlacedObject(currentPlacableObject, (int) currentCell.x, (int) currentCell.y, 1, 1);
					placedObjects.add(obj);
					currentOperation.targetObjects.add(obj);
				}
			} else if (input.getButton(Button.RIGHT)) {
				PlacedObject placed = getPlacedObject((int) currentCell.x, (int) currentCell.y);
				if (placed != null) {
					placedObjects.remove(placed);
					currentOperation.targetObjects.add(placed);
				}
			}
			if (input.getButtonReleased(Button.LEFT) || input.getButtonReleased(Button.RIGHT)) {
				addCurrentOperation();
			}
		} else if (currentMode == MODE_SELECT) {
			if (input.getButtonReleased(Button.LEFT)) {
				
			}
		}
		
		// Panel management
		if (input.getKeyPressed(Key.TAB)) {
			panelObjectsOpened = !panelObjectsOpened;
			panelLayersOpened = !panelLayersOpened;
		}
		
		// Undo/Redo
		if (input.getKeyPressed(Key.Z) && input.getKey(Key.LEFT_CONTROL)) {
			undo();
		}
		if (input.getKeyPressed(Key.Y) && input.getKey(Key.LEFT_CONTROL)) {
			redo();
		}
		
		if (input.getKeyPressed(Key.S) && input.getKey(Key.LEFT_CONTROL)) {
			saveLevel();
		}
		if (input.getKeyPressed(Key.L) && input.getKey(Key.LEFT_CONTROL)) {
			loadLevel();
		}
		
	}

	@Override
	public void render(Game game, GraphicsRenderer g, View view) {
		
		// Draw background and room grid
		g.drawRectangle(view.getX(), view.getY(), view.getWidth(), view.getHeight(), Color.GRAY);
		for (int x = 0; x < gridWidth + 1; x++) {
			g.drawLine(x * cellWidth, 0, x * cellWidth, roomHeight, Color.WHITE);
		}
		for (int y = 0; y < gridHeight + 1; y++) {
			g.drawLine(0, y * cellHeight, roomWidth, y * cellHeight, Color.WHITE);
		}
		
		// Draw placed objects
		for (PlacedObject placedObject : placedObjects) {
			g.drawRectangle(placedObject.x * cellWidth, placedObject.y * cellHeight, placedObject.width * cellWidth, placedObject.height * cellHeight, Color.RED);
		}
		
		// Draw current mode
		if (currentMode == MODE_PLACE) {
			g.drawRectangle(currentCell.x * cellWidth, currentCell.y * cellHeight, cellWidth, cellHeight, Color.WHITE);
		} else if (currentMode == MODE_SELECT) {
			if (mouseLeftIsDown) {
				CRectangle r = new CRectangle(previousCell, currentCell);
				if (previousCell.x != currentCell.x) {
					r.width += 1;
				}
				if (previousCell.y != currentCell.y) {
					r.height += 1;
				}
				g.drawRectangle(r.x * cellWidth, r.y * cellHeight, r.width * cellWidth, r.height * cellHeight, new Color(1.0f, 1.0f, 1.0f, 0.5f));
			}
		}
		
		// Draw menu bar
		g.drawRectangle(view.getX(), view.getY(), view.getWidth(), MENU_HEIGHT, Color.BLACK);
		
		// Draw objects panel
		if (panelObjectsOpened) {
			int x = view.getX();
			int y = view.getY() + MENU_HEIGHT;
			int width = PANEL_OBJECTS_WIDTH;
			int height = view.getHeight() - MENU_HEIGHT;
			g.drawRectangle(x, y, width, height, Color.BLACK);
			for (int i = 0; i < height / 32; i++) {
				g.drawLine(x, y, x + width, y);
			}
		}
		
		// Draw layers panel
		if (panelLayersOpened) {
			g.drawRectangle(view.getX() + view.getWidth() - PANEL_LAYERS_WIDTH, view.getY() + MENU_HEIGHT, PANEL_LAYERS_WIDTH, view.getHeight() - MENU_HEIGHT, Color.BLACK);
		}
		
	}
	
	private boolean getCellsFree(int x, int y, int width, int height) {
		CRectangle r = new CRectangle(x, y, width, height);
		CRectangle rPlaced = new CRectangle(0, 0, 0, 0);
		for (PlacedObject placedObject : placedObjects) {
			rPlaced.setPosition(placedObject.x, placedObject.y);
			rPlaced.setSize(placedObject.width, placedObject.height);
			if (r.collides(rPlaced)) {
				return false;
			}
		}
		return true;
	}
	
	private PlacedObject getPlacedObject(int x, int y) {
		CPoint p = new CPoint(x, y);
		CRectangle rPlaced = new CRectangle();
		for (PlacedObject placedObject : placedObjects) {
			rPlaced.reset(placedObject.x, placedObject.y, placedObject.width, placedObject.height);
			if (rPlaced.collides(p)) {
				return placedObject;
			}
		}
		return null;
	}
	
	private PlacedObject getPlacedObjectCorner(int x, int y) {
		for (PlacedObject placedObject : placedObjects) {
			if (placedObject.x == x && placedObject.y == y) {
				return placedObject;
			}
		}
		return null;
	}
	
	private void newOperation(int type) {
		addCurrentOperation();
		currentOperation = new Operation(type);
	}
	
	private void addCurrentOperation() {
		if (currentOperation != null && currentOperation.targetObjects.size() > 0) {
			while (operations.size() > operationIndex + 1) {
				operations.remove(operations.size() - 1);
			}
			operations.add(currentOperation);
			operationIndex = operations.size() - 1;
			currentOperation = new Operation(-1);
		}
	}
	
	private void undo() {
		if (operationIndex >= 0) {
			Operation operation = operations.get(operationIndex);
			switch (operation.type) {
			case OPERATION_ADD_OBJECTS:
				for (PlacedObject obj : operation.targetObjects) {
					placedObjects.remove(obj);
				}
				break;
			case OPERATION_REMOVE_OBJECTS:
				for (PlacedObject obj : operation.targetObjects) {
					placedObjects.add(obj);
				}
				break;
			}
			operationIndex--;
		}
	}
	
	private void redo() {
		if (operationIndex < operations.size() - 1) {
			Operation operation = operations.get(operationIndex + 1);
			switch (operation.type) {
			case OPERATION_ADD_OBJECTS:
				for (PlacedObject obj : operation.targetObjects) {
					placedObjects.add(obj);
				}
				break;
			case OPERATION_REMOVE_OBJECTS:
				for (PlacedObject obj : operation.targetObjects) {
					placedObjects.remove(obj);
				}
				break;
			}
			operationIndex++;
		}
	}
	
	private void saveLevel() {
		if (saveFile == null) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileFilter(filter);
			int result = fileChooser.showSaveDialog(null);
			if (result == JFileChooser.CANCEL_OPTION) {
				return;
			}
			saveFile = fileChooser.getSelectedFile().getAbsolutePath();
		}
		TextFile textFile = new TextFile(saveFile);
		textFile.clearLines();
		// Loop all layers
		textFile.insertLine("//LAYER=WALLS");
		// Test layer type
		textFile.insertLine("//LAYERTYPE=GRID");
		textFile.insertLine("//GRIDWIDTH=" + gridWidth);
		textFile.insertLine("//GRIDHEIGHT=" + gridHeight);
		textFile.insertLine("//CELLWIDTH=" + cellWidth);
		textFile.insertLine("//CELLHEIGHT=" + cellHeight);
		// Loop all objects per layer
		PlacedObject placedObject;
		String line;
		for (int y = 0; y < gridHeight; y++) {
			line = "";
			for (int x = 0; x < gridWidth; x++) {
				placedObject = getPlacedObjectCorner(x, y);
				if (placedObject == null) {
					line += "0;";
				} else {
					line += placedObject.getID() + ";";
				}
			}
			line = line.substring(0, line.length() - 1);
			textFile.insertLine(line);
		}
		textFile.close();
	}
	
	private void loadLevel() {
		// Warn the user about his map being cleared
		// Clear all placed objects per layer
		placedObjects.clear();
		// Open file and add objects
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(filter);
		int result = fileChooser.showOpenDialog(null);
		if (result == JFileChooser.CANCEL_OPTION) {
			return;
		}
		TextFile textFile = new TextFile(fileChooser.getSelectedFile().getAbsolutePath());
		String line;
		int x = 0;
		int y = 0;
		while ((line = textFile.readLine()) != null) {
			if (line.startsWith("//")) {
				// COMMAND
			} else {
				x = 0;
				String[] split = line.split(";");
				for (String str : split) {
					if (Integer.parseInt(str) == 1) {
						PlacedObject placedObject = new PlacedObject(currentPlacableObject, x, y, 1, 1);
						placedObjects.add(placedObject);
					}
					x++;
				}
				y++;
			}
		}
	}

}
