package com.adx.adx3engine.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.adx.adx3engine.frame.Game;
import com.adx.adx3engine.frame.Instance;
import com.adx.adx3engine.io.InputManager;
import com.adx.adx3engine.render.GraphicsRenderer;
import com.adx.adx3engine.render.View;

public abstract class OldNetClient extends Instance {

	public static final byte PACKET_CONNECT = 100;
	public static final byte PACKET_DISCONNECT = 101;
	public static final byte PACKET_PING = 102;

	private Game game;
	private short index = -1;
	private boolean connect = false;
	private InetAddress address;
	private int port;
	private DatagramSocket socket;
	private ListenThread listen;
	private NetData buffer;

	public abstract void onConnected();
	public abstract void onPacketReceived(NetData data);
	public abstract void onDisconnected();

	public boolean isConnected() {
		return connect;
	}
	
	@Override
	public void load(Game game) {
		this.game = game;
		buffer = new NetData(1024);
	}

	public void connect(String ip, int port) {
		if (connect) {
			return;
		}
		try {
			address = InetAddress.getByName(ip);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		this.port = port;
		game.printString("Connecting to server " + address.getHostAddress() + ":" + port + "...");
		try {
			socket = new DatagramSocket();
		} catch (SocketException e) {
			game.printError("Could not connect to the server.");
			return;
		}
		listen = new ListenThread(this);
		listen.start();
		byte[] b = { (byte)0xFF, (byte)0xFF, PACKET_CONNECT };
		DatagramPacket packet = new DatagramPacket(b, b.length, address, port);
		try {
			socket.send(packet);
		} catch (IOException e) {
			game.printError("Could not send the connection packet.");
			return;
		}
	}

	public void abord() {
		if (connect) {
			return;
		}
		connect = false;
		if (listen != null) {
			listen.run = false;
		}
		if (socket != null) {
			socket.close();
		}
	}
	
	public void disconnect() {
		if (!connect) {
			return;
		}
		connect = false;
		NetData data = new NetData();
		data.writeByte(PACKET_DISCONNECT);
		send(data);
		flushData();
		listen.run = false;
		socket.close();
		game.printString("Connection successfully closed.");
	}

	public void send(NetData data) {
		buffer.append(data);
	}
	
	public void flushData() {
		if (index == -1 || buffer.getPosition() == 0) {
			return;
		}
		byte[] b = new byte[buffer.getPosition() + 2];
		b[0] = (byte) ((index >> 8) & 0xFF);
		b[1] = (byte) (index & 0xFF);
		System.arraycopy(buffer.getBytes(), 0, b, 2, buffer.getPosition());
		DatagramPacket packet = new DatagramPacket(b, b.length, address, port);
		try {
			socket.send(packet);
			buffer.setPosition(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void update(Game game, InputManager input) {
		flushData();
	}
	
	@Override
	public void render(Game game, GraphicsRenderer g, View view) {
	}

	private class ListenThread extends Thread {

		public boolean run = true;
		private OldNetClient client;

		public ListenThread(OldNetClient oldNetClient) {
			this.client = oldNetClient;
		}

		@Override
		public void run() {

			byte[] buf = new byte[256];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);

			while (run) {

				try {
					socket.receive(packet);
				} catch (IOException e) {
					client.disconnect();
					return;
				}

				NetData data = new NetData(packet.getData(), packet.getLength());
				int type = data.readByte();
				if (type == PACKET_CONNECT) {
					index = data.readShort();
					connect = true;
					game.printString("Connection successful.");
					onConnected();
				} else if (type == PACKET_DISCONNECT) {
					game.printError("The server closed the connection.");
					onDisconnected();
				} else if (type == PACKET_PING) {
					NetData d = new NetData();
					d.writeByte(PACKET_PING);
					client.send(d);
				} else {
					data.setPosition(0);
					onPacketReceived(data);
				}

			}

		}

	}

}
