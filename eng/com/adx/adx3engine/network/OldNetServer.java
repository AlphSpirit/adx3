package com.adx.adx3engine.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import com.adx.adx3engine.frame.Game;
import com.adx.adx3engine.frame.Instance;
import com.adx.adx3engine.io.InputManager;
import com.adx.adx3engine.render.GraphicsRenderer;
import com.adx.adx3engine.render.View;

public abstract class OldNetServer extends Instance {

	public static final byte PACKET_CONNECT = 100;
	public static final byte PACKET_DISCONNECT = 101;
	public static final byte PACKET_PING = 102;

	private Game game;
	private boolean open = false;
	private int port;
	private ListenThread listen;
	private PingThread ping;
	private Client[] clients;
	private DatagramSocket socket;

	public class Client {

		private NetData buffer;
		private int index;
		private InetAddress address;
		private int port;
		private int ping = 0;
		private int currentPing = 0;
		private int accPing = 0;

		public Client(int index, InetAddress address, int port) {
			this.index = index;
			this.address = address;
			this.port = port;
			buffer = new NetData(1024);
		}

		public int getIndex() {
			return index;
		}

		public int getPing() {
			return ping;
		}

		public String getIP() {
			return address.toString();
		}
		
		public void flushData() {
			if (buffer.getPosition() == 0) {
				return;
			}
			System.out.println(index + ";" + buffer);
			DatagramPacket packet = new DatagramPacket(buffer.getBytes(), buffer.getPosition(), address, port);
			try {
				socket.send(packet);
				buffer.setPosition(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public abstract void clientConnected(Client c);
	public abstract void clientDisconnected(Client c);
	public abstract void packetReceived(Client c, NetData data);

	public int getPort() {
		return port;
	}
	
	public int getMaxPlayers() {
		return clients.length;
	}

	public Client getClient(int index) {
		return clients[index];
	}
	
	@Override
	public void load(Game game) {
		this.game = game;
	}

	public void open(int port, int maxPlayers) {
		if (open) {
			return;
		}
		this.port = port;
		clients = new Client[maxPlayers];
		game.printString("Opening net server on port " + port + "...");
		try {
			socket = new DatagramSocket(port);
		} catch (SocketException e) {
			game.printError("Could not open server.");
			return;
		}
		listen = new ListenThread(this);
		listen.start();
		ping = new PingThread(this);
		ping.start();
		open = true;
		game.printString("Server opened.");
	}

	public void close() {
		if (!open) {
			return;
		}
		open = false;
		listen.run = false;
		ping.run = false;
		socket.close();
		game.printString("Server successfully closed.");
	}

	public void send(Client c, NetData data) {
		c.buffer.append(data);
	}

	public void broadcast(NetData data) {
		broadcast(data, null);
	}

	public void broadcast(NetData data, Client exception) {
		Client c;
		for (int i = 0; i < clients.length; i++) {
			c = clients[i];
			if (c != null && c != exception) {
				send(c, data);
			}
		}
	}

	private void kickClient(int client) {
		clientDisconnected(clients[client]);
		clients[client] = null;
	}
	
	public void flushData() {
		Client c;
		for (int i = 0; i < clients.length; i++) {
			c = clients[i];
			if (c != null) {
				c.flushData();
			}
		}
	}
	
	@Override
	public void update(Game game, InputManager input) {
		flushData();
	}
	
	@Override
	public void render(Game game, GraphicsRenderer g, View view) {
	}

	private class ListenThread extends Thread {

		public boolean run = true;
		private OldNetServer server;

		public ListenThread(OldNetServer oldNetServer) {
			this.server = oldNetServer;
		}

		@Override
		public void run() {

			byte[] buf = new byte[1024];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);

			while (run) {

				try {
					socket.receive(packet);
				} catch (IOException e) {
					server.close();
					return;
				}

				InetAddress address = packet.getAddress();
				int port = packet.getPort();
				NetData data = new NetData(packet.getData(), packet.getLength());
				int client = data.readShort();
				if (client == -1 || clients[client] != null) {
					int type = data.readByte();
					if (type == PACKET_CONNECT) {
						for (int i = 0; i < clients.length; i++) {
							if (clients[i] == null) {
								clients[i] = new Client(i, address, port);
								NetData d = new NetData();
								d.writeByte(PACKET_CONNECT);
								d.writeShort((byte) i);
								clients[i].flushData();
								server.send(clients[i], d);
								clients[i].flushData();
								clientConnected(clients[i]);
								break;
							}
						}
					} else if (type == PACKET_DISCONNECT) {
						kickClient(client);
					} else if (type == PACKET_PING) {
						clients[client].ping = clients[client].currentPing / 2;
						clients[client].currentPing = 0;
						clients[client].accPing = 0;
					} else {
						byte[] b = new byte[packet.getLength() - 2];
						System.arraycopy(packet.getData(), 2, b, 0, packet.getLength() - 2);
						NetData d = new NetData(b, b.length);
						packetReceived(clients[client], d);
					}
				}

			}

		}

	}

	private class PingThread extends Thread {

		public boolean run = true;
		private OldNetServer server;

		public PingThread(OldNetServer oldNetServer) {
			this.server = oldNetServer;
		}

		@Override
		public void run() {

			long timeThen = System.nanoTime();
			long timeNow;
			long timeDiff = 0;
			NetData data = new NetData();
			data.writeByte(PACKET_PING);
			int loop = 60;

			while (run) {

				timeNow = System.nanoTime();
				timeDiff += timeNow - timeThen;

				if (timeDiff > 1000000000 / 60) {
					loop++;
					timeDiff = 0;
					for (int i = 0; i < clients.length; i++) {
						if (clients[i] != null) {
							clients[i].currentPing++;
							clients[i].accPing++;
							if (clients[i].accPing > 60 * 10) {
								kickClient(i);
							}
						}
					}
				}
				if (loop >= 60) {
					for (int i = 0; i < clients.length; i++) {
						if (clients[i] != null) {
							clients[i].currentPing = 0;
						}
					}
					server.flushData();
					server.broadcast(data);
					server.flushData();
					loop = 0;
				}

				timeThen = timeNow;

			}

		}

	}

}
