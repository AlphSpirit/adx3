package algorithms;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import collision.CRectangle;

import draw.Draw;
import draw.TTFont;
import environment.WallControl;

@SuppressWarnings("deprecation")
public class AStar {

	private int xEnd = 0;
	private int yEnd = 0;
	public boolean ended = false;
	private List<Node> list = new ArrayList<Node>();
	public int steps = 0;
	private int stepsNow = 0;

	private TrueTypeFont smallFont = TTFont.get("fnt16");

	public AStar(int xStart, int yStart, int xEnd, int yEnd) {

		this.xEnd = xEnd;
		this.yEnd = yEnd;
		list.add(new Node(xStart, yStart, null));

	}

	public Node addNode(int x, int y, Node parent) {

		Node n = new Node(x, y, parent);

		list.add(n);

		return n;

	}

	public Node getNode(int x, int y) {

		for (Node n : list) {

			if (n.x == x && n.y == y) {

				return n;

			}

		}

		return null;

	}

	public List<Point> getPath() {

		List<Point> points = new ArrayList<Point>();

		Node node = null;
		int score = 1000000000;

		for (Node n : list) {

			if (n.open && n.f < score) {
				node = n;
				score = n.f;
			}

		}

		if (node != null) {

			while (node.parent != null) {

				points.add(new Point(node.x * 32 + 16, node.y * 32 + 16));
				node = node.parent;

			}

		}

		return points;

	}

	public void takeStep() {

		Node n;
		int jMin;
		int kMin;
		int jMax;
		int kMax;
		stepsNow++;

		for (int i = list.size() - 1; i >= 0; i--) {

			n = list.get(i);

			if (n.open) {

				jMin = -1;
				kMin = -1;
				jMax = 1;
				kMax = 1;

				if (getNode(n.x - 1, n.y) != null || WallControl.collision(new CRectangle((n.x - 1) * 32, (n.y) * 32, 32, 32))) {
					jMin = 0;
				}
				if (getNode(n.x + 1, n.y) != null || WallControl.collision(new CRectangle((n.x + 1) * 32, (n.y) * 32, 32, 32))) {
					jMax = 0;
				}
				if (getNode(n.x, n.y - 1) != null || WallControl.collision(new CRectangle((n.x) * 32, (n.y - 1) * 32, 32, 32))) {
					kMin = 0;
				}
				if (getNode(n.x, n.y + 1) != null || WallControl.collision(new CRectangle((n.x) * 32, (n.y + 1) * 32, 32, 32))) {
					kMax = 0;
				}

				for (int j = jMin; j <= jMax; j++) {

					for (int k = kMin; k <= kMax; k++) {

						Node n2 = getNode(n.x + j, n.y + k);
						if (n2 == null) {

							if (!WallControl.collision(new CRectangle((n.x + j) * 32, (n.y + k) * 32, 32, 32))) {

								n2 = addNode(n.x + j, n.y + k, n);
								n2.calculateVariables();

							}

						} else {

						}

					}

				}

				if (n != null) {
					n.close();
				}

			}

		}

		boolean oneOpen = false;

		for (Node no : list) {

			if (no.open) {
				oneOpen = true;
			}

			if (no.x == xEnd && no.y == yEnd) {

				steps = stepsNow;
				stepsNow = 0;
				ended = true;

			}

		}

		if (!oneOpen) {
			ended = true;
		}

	}

	public void render() {

		Node node = null;
		int score = 1000000000;

		for (Node n : list) {

			if (n.open && n.f < score) {

				node = n;
				score = n.f;

			}

			// Color colorDraw;
			//
			// if (n.open) {
			// colorDraw = Color.green;
			// } else {
			// colorDraw = Color.red;
			// }
			//
			// Draw.rectangle(n.x * 32, n.y * 32, 32, 32, colorDraw);

		}

		if (node != null) {

			while (node.parent != null) {

				Draw.rectangle(node.x * 32, node.y * 32, 32, 32, Color.yellow);
				Draw.text("f:" + node.f, node.x * 32, node.y * 32, smallFont, Color.black, TTFont.LEFT, TTFont.TOP, 1);
				node = node.parent;

			}

		}

	}

	private class Node {

		int x = 0;
		int y = 0;
		int f = 0;
		int g = 0;
		int h = 0;
		boolean open = true;
		Node parent = null;

		public Node(int x, int y, Node parent) {

			this.x = x;
			this.y = y;
			this.parent = parent;
			open = true;

		}

		public void close() {

			open = false;

		}

		public void calculateVariables() {

			int man1 = getManDistance(x, y, parent.x, parent.y);
			int man2 = getManDistance(x, y, xEnd, yEnd);

			g = parent.g + 10 * (int) Math.sqrt(man1);
			h = 15 * (man2);
			f = g + h;

		}
	}

	public static int getManDistance(int x1, int y1, int x2, int y2) {

		int man = 0;

		if (x2 > x1) {
			man += x2 - x1;
		} else {
			man += x1 - x2;
		}

		if (y2 > y1) {
			man += y2 - y1;
		} else {
			man += y1 - y2;
		}

		return man;

	}

}
