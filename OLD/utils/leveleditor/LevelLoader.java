package com.adx.eng.utils.leveleditor;

import com.adx.eng.io.XMLFile;

public abstract class LevelLoader {
	
	public void loadLevel(String path) {
		
		/*TextFile file = new TextFile(path);
		
		String line;
		String[] split;
		while ((line = file.readLine()) != null) {
			if (line.charAt(0) != '*') {
				// Place instance
				split = line.split(";");
				placeObject(Integer.parseInt(split[0]),
						Integer.parseInt(split[1]),
						Integer.parseInt(split[2]),
						Integer.parseInt(split[3]),
						Integer.parseInt(split[4]));
			}
		}*/
		
		XMLFile file = new XMLFile(path, true);
		file.getMasterNode();
		
	}
	
	public abstract void placeObject(int id, int x, int y, int width, int height);

}
