package com.adx.eng.utils.render3d;

import java.util.ArrayList;

import com.adx.eng.maths.Transform;

public class Transform3D extends Transform {
	
	public static final int TRANSLATION = 1;
	public static final int SCALING = 2;
	public static final int ROTATION = 3;
	
	public ArrayList<Transformation> transformations = new ArrayList<Transformation>();
	
	private class Transformation {
		private int type;
		private float arg1;
		private float arg2;
		private float arg3;
		public Transformation(int type, float arg1, float arg2, float arg3) {
			this.type = type;
			this.arg1 = arg1;
			this.arg2 = arg2;
			this.arg3 = arg3;
		}
	}
	
	public void translate(float xPlus, float yPlus, float zPlus) {
		transformations.add(new Transformation(TRANSLATION, xPlus, yPlus, zPlus));
	}
	
	public void scale(float xScale, float yScale, float zScale) {
		transformations.add(new Transformation(SCALING, xScale, yScale, zScale));
	}
	
	public void scaleFrom(float x, float y, float z, float xScale, float yScale, float zScale) {
		translate(-x, -y, -z);
		scale(xScale, yScale, zScale);
		translate(x, y, -z);
	}
	
	/*public void rotate(float angle) {
		transformations.add(new Transformation(ROTATION, angle, 0));
	}*/
	
	public void remove() {
		if (transformations.size() > 0) {
			transformations.remove(transformations.size() - 1);
		}
	}
	
	public void remove(int quantity) {
		for (int i = 0; i < quantity; i++) {
			remove();
		}
	}
	
	public void clear() {
		transformations.clear();
	}
	
	public Vector3 applyTransformations(Vector3 v) {
		
		for (Transformation t : transformations) {
			
			switch (t.type) {
			
			case TRANSLATION:
				v.x += t.arg1;
				v.y += t.arg2;
				v.z += t.arg3;
				break;
				
			case SCALING:
				v.x *= t.arg1;
				v.y *= t.arg2;
				v.z *= t.arg3;
				break;
			
			}
			
		}
		
		return v;
		
	}

}
