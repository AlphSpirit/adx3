package com.adx.eng.utils.render3d;

import java.util.ArrayList;

import com.adx.eng.frame.Game;
import com.adx.eng.io.TextFile;
import com.adx.eng.maths.Vector;
import com.adx.eng.render.Color;
import com.adx.eng.render.Texture;

public class OBJModel {
	
	private ArrayList<Material> lMaterial = new ArrayList<Material>();
	private ArrayList<Vertex> lVertex = new ArrayList<Vertex>();
	private ArrayList<TextureCoord> lTextureCoord = new ArrayList<TextureCoord>();
	private ArrayList<Normal> lNormal = new ArrayList<Normal>();
	private ArrayList<Face> lFace = new ArrayList<Face>();
	
	private class Material {
		String name;
		Color color = null;
		Texture tex = null;
		public Material(String name) {
			this.name = name;
		}
		public void setColor(float r, float g, float b) {
			color = new Color(r, g, b);
		}
		public void setTexture(Game game, String path) {
			tex = new Texture(path);
		}
	}
	
	private class Face {
		
		private int[] vertexIndex;
		private int[] textureCoordIndex;
		private int[] normalIndex;
		private Material material = null;
		
		public Vector3 getVertex(int index) {
			Vertex v = lVertex.get(vertexIndex[index] - 1);
			return new Vector3(v.x, v.y, v.z);
		}
		
		public Vector3[] getAllVertices() {
			Vector3[] v = new Vector3[vertexIndex.length];
			for (int i = 0; i < vertexIndex.length; i++) {
				v[i] = getVertex(i);
			}
			return v;
		}
		
		public Vector getTextureCoord(int index) {
			TextureCoord c = lTextureCoord.get(textureCoordIndex[index] - 1);
			return new Vector(c.x, c.y);
		}
		
		public Vector[] getAllTextureCoords() {
			Vector[] v = new Vector[textureCoordIndex.length];
			for (int i = 0; i < textureCoordIndex.length; i++) {
				v[i] = getTextureCoord(i);
			}
			return v;
		}
		
		public Vector3 getNormal(int index) {
			Normal n = lNormal.get(normalIndex[index] - 1);
			return new Vector3(n.x, n.y, n.z);
		}
		
		public Vector3[] getAllNormals() {
			Vector3[] v = new Vector3[normalIndex.length];
			for (int i = 0; i < normalIndex.length; i++) {
				v[i] = getNormal(i);
			}
			return v;
		}
		
		public void setMaterial(Material material) {
			this.material = material;
		}
		
		public void setVertexCount(int number) {
			vertexIndex = new int[number];
			textureCoordIndex = new int[number];
			normalIndex = new int[number];
		}
		
		public void addInfo(int index, int v, int t, int n) {
			vertexIndex[index] = v;
			textureCoordIndex[index] = t;
			normalIndex[index] = n;
		}
		
	}
	
	private class Vertex {
		private float x;
		private float y;
		private float z;
		public Vertex(float x, float y, float z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
	
	private class TextureCoord {
		private float x;
		private float y;
		public TextureCoord(float x, float y) {
			this.x = x;
			this.y = y;
		}
	}
	
	private class Normal {
		private float x;
		private float y;
		private float z;
		public Normal(float x, float y, float z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
	
	public OBJModel(Game game, String path, String fileName) {
		
		String line;
		String[] split;
		
		// Load materials
		TextFile txtMtl = new TextFile(path + "/" + fileName + ".mtl");
		Material material = null;
		while ((line = txtMtl.readLine()) != null) {
			if (line.length() > 0) {
				split = line.split(" ");
				if (split[0].equals("newmtl")) {
					if (material != null) {
						lMaterial.add(material);
					}
					material = new Material(split[1]);
				} else if (split[0].equals("Kd")) {
					material.setColor(getFloat(split[1]), getFloat(split[2]), getFloat(split[3]));
				} else if (split[0].equals("map_Kd")) {
					material.setTexture(game, path + "/" + split[1]);
				}
			}
		}
		lMaterial.add(material);
		
		// Load mesh
		TextFile txtObj = new TextFile(path + "/" + fileName + ".obj");
		Face currentFace = new Face();
		Material currentMaterial = null;
		while ((line = txtObj.readLine()) != null) {
			if (line.length() > 0) {
				split = line.split(" ");
				if (split[0].equals("usemtl")) {
					currentMaterial = getMaterial(split[1]);
				} else if (split[0].equals("v")) {
					lVertex.add(new Vertex(getFloat(split[1]), getFloat(split[3]), getFloat(split[2])));
				} else if (split[0].equals("vt")) {
					lTextureCoord.add(new TextureCoord(getFloat(split[1]), getFloat(split[2])));
				} else if (split[0].equals("vn")) {
					lNormal.add(new Normal(getFloat(split[1]), getFloat(split[2]), getFloat(split[3])));
				} else if (split[0].equals("f")) {
					int vertices = split.length - 1;
					currentFace.setMaterial(currentMaterial);
					currentFace.setVertexCount(vertices);
					String[] resplit;
					for (int i = 0; i < vertices; i++) {
						resplit = split[i + 1].split("/");
						currentFace.addInfo(i, getInt(resplit[0]), getInt(resplit[1]), getInt(resplit[2]));
					}
					lFace.add(currentFace);
					currentFace = new Face();
				}
			}
		}
		
		System.out.println("New model with " + lFace.size() + " faces.");
		
	}
	
	public void render(OpenGLLegacy3DRenderer g) {
		for (Face face : lFace) {
			if (face.material.tex == null) {
				g.setColor(face.material.color);
				g.drawPolygonNormal(face.getAllVertices(), face.getAllNormals());
			} else {
				g.setColor(face.material.color);
				g.drawPolygonTexturedNormal(face.material.tex, face.getAllVertices(), face.getAllTextureCoords(), face.getAllNormals());
			}
		}
	}
	
	public int getInt(String str) {
		return Integer.parseInt(str);
	}
	
	public float getFloat(String str) {
		return Float.parseFloat(str);
	}
	
	public Material getMaterial(String name) {
		for (Material material : lMaterial) {
			if (material.name.equals(name)) {
				return material;
			}
		}
		return null;
	}

}
