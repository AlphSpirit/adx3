package com.adx.eng.utils.render3d;

import com.adx.eng.frame.Game;
import com.adx.eng.maths.Vector;
import com.adx.eng.render.OpenGLLegacyRenderer;
import com.adx.eng.render.Texture;

import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL11.*;

public class OpenGLLegacy3DRenderer extends OpenGLLegacyRenderer {

	private static final float[] IDENTITY_MATRIX = new float[] { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
	private final FloatBuffer matrix = BufferUtils.createFloatBuffer(16);
	private final float[] forward = new float[3];
	private final float[] side = new float[3];
	private final float[] up = new float[3];

	// public Transform3D transform = new Transform3D();

	public OpenGLLegacy3DRenderer(Game game, int id) {
		super(game, id);
		this.transform = new Transform3D();
	}

	@Override
	public void init() {

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(45.0f, game.getWidth() / (float) game.getHeight(), 0.1f, 10000.0f);
		glScalef(-1, 1, 1);
		glEnable(GL_CULL_FACE);

	}
	
	@Override
	public void deinit() {
		glDisable(GL_CULL_FACE);
	}

	public void drawLine(float x1, float y1, float z1, float x2, float y2, float z2) {
		
		glBindTexture(GL_TEXTURE_2D, texWhite.getID());
		glBegin(GL_LINES);
		glVertex3f(x1, y1, z1);
		glVertex3f(x2, y2, z2);
		glEnd();
		
	}

	public void drawTriangle(Vector3 pos1, Vector3 pos2, Vector3 pos3) {
		
		glBindTexture(GL_TEXTURE_2D, texWhite.getID());
		glBegin(GL_TRIANGLES);
		vertex(pos1);
		vertex(pos2);
		vertex(pos3);
		glEnd();
		
	}
	
	public void drawRectangle(Vector3 pos1, Vector3 pos2, Vector3 pos3, Vector3 pos4) {
		
		glBindTexture(GL_TEXTURE_2D, texWhite.getID());
		glBegin(GL_QUADS);
		vertex(pos1);
		vertex(pos2);
		vertex(pos3);
		vertex(pos4);
		glEnd();
		
	}

	public void drawFloor(float x, float y, float z, float width, float height) {

		glBindTexture(GL_TEXTURE_2D, texWhite.getID());
		glBegin(GL_QUADS);
		glVertex3f(x, y + height, z);
		glVertex3f(x + width, y + height, z);
		glVertex3f(x + width, y, z);
		glVertex3f(x, y, z);
		glEnd();

	}
	public void drawFloor(float x, float y, float z, float width, float height,Texture tex) {
		
		//checkShapeTexture(GL_QUADS, tex.getID());
		glBindTexture(GL_TEXTURE_2D, tex.getID());
		float x1  =  width/ tex.getWidth();
		float y1 = height/ tex.getHeight();
		glBegin(GL_QUADS);
		glNormal3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(0, 0);
		glVertex3f(x, y + height, z);
		glTexCoord2f(x1, 0);
		glVertex3f(x + width, y + height, z);
		glTexCoord2f(x1, y1);
		glVertex3f(x + width, y, z);
		glTexCoord2f(0,y1);
		glVertex3f(x, y, z);
		glEnd();

	}
	
	public void drawWall(float x1, float y1, float z1, float x2, float y2, float z2) {

		glBindTexture(GL_TEXTURE_2D, texWhite.getID());
		glBegin(GL_QUADS);
		glVertex3f(x1, y1, z1);
		glVertex3f(x2, y2, z1);
		glVertex3f(x2, y2, z2);
		glVertex3f(x1, y1, z2);
		glEnd();

	}
	
	public void drawWallTexture(Texture tex, float x1, float y1, float z1, float x2, float y2, float z2) {

		glBindTexture(GL_TEXTURE_2D, tex.getID());
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex3f(x1, y1, z1);
		glTexCoord2f(1, 0);
		glVertex3f(x2, y2, z1);
		glTexCoord2f(1, 1);
		glVertex3f(x2, y2, z2);
		glTexCoord2f(0, 1);
		glVertex3f(x1, y1, z2);
		glEnd();

	}
	
	public void drawWallTextureRepeat(Texture tex, float repeatX, float repeatY, float x1, float y1, float z1, float x2, float y2, float z2) {

		glBindTexture(GL_TEXTURE_2D, tex.getID());
		glBegin(GL_QUADS);
		glTexCoord2f(0, repeatY);
		glVertex3f(x1, y1, z1);
		glTexCoord2f(repeatX, repeatY);
		glVertex3f(x2, y2, z1);
		glTexCoord2f(repeatX, 0);
		glVertex3f(x2, y2, z2);
		glTexCoord2f(0, 0);
		glVertex3f(x1, y1, z2);
		glEnd();

	}

	public void drawCube(float x, float y, float z, float width, float height, float depth) {

		glBindTexture(GL_TEXTURE_2D, texWhite.getID());
		glBegin(GL_QUADS);
		// Bottom face
		glVertex3f(x, y, z);
		glVertex3f(x + width, y, z);
		glVertex3f(x + width, y + height, z);
		glVertex3f(x, y + height, z);
		// Top face
		glVertex3f(x, y + height, z + depth);
		glVertex3f(x + width, y + height, z + depth);
		glVertex3f(x + width, y, z + depth);
		glVertex3f(x, y, z + depth);
		// Left face
		glVertex3f(x, y, z);
		glVertex3f(x, y + height, z);
		glVertex3f(x, y + height, z + depth);
		glVertex3f(x, y, z + depth);
		// Right face
		glVertex3f(x + width, y + height, z);
		glVertex3f(x + width, y, z);
		glVertex3f(x + width, y, z + depth);
		glVertex3f(x + width, y + height, z + depth);
		// Back face
		glVertex3f(x, y, z + depth);
		glVertex3f(x + width, y, z + depth);
		glVertex3f(x + width, y, z);
		glVertex3f(x, y, z);
		// Front face
		glVertex3f(x, y + height, z);
		glVertex3f(x + width, y + height, z);
		glVertex3f(x + width, y + height, z + depth);
		glVertex3f(x, y + height, z + depth);
		glEnd();

	}
	
	public void drawSphere(float x, float y, float z, float radius) {
		
		int precision = 32;
		float topZ;
		float botZ;
		float topRad;
		float botRad;
		float angle1;
		float angle2;
		glBindTexture(GL_TEXTURE_2D, texWhite.getID());
		glBegin(GL_QUADS);
		for (int i = 0; i < precision; i++) {
			topZ = (i / (float) precision) * 2 * radius + z - radius;
			botZ = ((i + 1) / (float) precision) * 2 * radius + z - radius;
			topRad = (float) Math.sin(Math.acos(i / (float) precision * 2 - 1)) * radius;
			botRad = (float) Math.sin(Math.acos((i + 1) / (float) precision * 2 - 1)) * radius;
			for (int j = 0; j < precision; j++) {
				angle1 = (float) ((j / (float) precision) * 2 * Math.PI);
				angle2 = (float) (((j + 1) / (float) precision) * 2 * Math.PI);
				Vector3 v1 = new Vector3((float) Math.cos(angle2) * topRad + x, (float) Math.sin(angle2) * topRad + y, topZ);
				Vector3 v2 = new Vector3((float) Math.cos(angle1) * topRad + x, (float) Math.sin(angle1) * topRad + y, topZ);
				Vector3 v3 = new Vector3((float) Math.cos(angle1) * botRad + x, (float) Math.sin(angle1) * botRad + y, botZ);
				Vector3 v4 = new Vector3((float) Math.cos(angle2) * botRad + x, (float) Math.sin(angle2) * botRad + y, botZ);
				Vector3 vC1 = v2.minus(v1);
				Vector3 vC2 = v3.minus(v1);
				Vector3 normal = vC2.cross(vC1);
				glNormal3f(normal.x, normal.y, normal.z);
				vertex(v1);
				vertex(v2);
				vertex(v3);
				vertex(v4);
			}
		}
		glEnd();
		
	}
	
	public void drawCylinder(float x, float y, float z, float radius, float height) {
		
		int precision = 32;
		glBindTexture(GL_TEXTURE_2D, texWhite.getID());
		// Bottom circle
		glBegin(GL_TRIANGLE_FAN);
		glNormal3f(0, 0, -1);
		glVertex3f(x, y, z);
		float angle;
		for (int i = 0; i < precision + 1; i++) {
			angle = (float) Math.PI * 2 * (i / (float) precision);
			glVertex3f(x + radius * (float) Math.cos(angle), y + radius * (float) Math.sin(angle), z);
		}
		glEnd();
		// Top Circle
		glBegin(GL_TRIANGLE_FAN);
		glNormal3f(0, 0, 1);
		glVertex3f(x, y, z + height);
		for (int i = 0; i < precision + 1; i++) {
			angle = (float) -Math.PI * 2 * (i / (float) precision);
			glVertex3f(x + radius * (float) Math.cos(angle), y + radius * (float) Math.sin(angle), z + height);
		}
		glEnd();
		// Sides
		glBegin(GL_QUADS);
		float angle2;
		for (int i = 0; i < precision + 1; i++) {
			angle = (float) Math.PI * 2 * (i / (float) precision);
			angle2 = (float) Math.PI * 2 * ((i + 1) / (float) precision);
			Vector3 v1 = new Vector3(x + radius * (float) Math.cos(angle), y + radius * (float) Math.sin(angle), z + height);
			Vector3 v2 = new Vector3(x + radius * (float) Math.cos(angle2), y + radius * (float) Math.sin(angle2), z + height);
			Vector3 v3 = new Vector3(x + radius * (float) Math.cos(angle2), y + radius * (float) Math.sin(angle2), z);
			Vector3 v4 = new Vector3(x + radius * (float) Math.cos(angle), y + radius * (float) Math.sin(angle), z);
			Vector3 vC1 = v2.minus(v1);
			Vector3 vC2 = v3.minus(v1);
			Vector3 normal = vC2.cross(vC1);
			glNormal3f(normal.x, normal.y, normal.z);
			vertex(v1);
			vertex(v2);
			vertex(v3);
			vertex(v4);
		}
		glEnd();
		
	}
	
	public void drawCone(float x, float y, float z, float radius, float height) {
		
		int precision = 32;
		glBindTexture(GL_TEXTURE_2D, texWhite.getID());
		// Bottom circle
		glBegin(GL_TRIANGLE_FAN);
		glNormal3f(0, 0, -1);
		glVertex3f(x, y, z);
		float angle;
		for (int i = 0; i < precision + 1; i++) {
			angle = (float) Math.PI * 2 * (i / (float) precision);
			glVertex3f(x + radius * (float) Math.cos(angle), y + radius * (float) Math.sin(angle), z);
		}
		glEnd();
		// Sides
		glBegin(GL_TRIANGLE_FAN);
		Vector3 v1 = new Vector3(x, y, z + height);
		vertex(v1);
		float angle2;
		for (int i = 0; i < precision + 1; i++) {
			angle = (float) Math.PI * 2 * (i / (float) precision);
			angle2 = (float) Math.PI * 2 * ((i + 1) / (float) precision);
			Vector3 v2 = new Vector3(x + radius * (float) Math.cos(angle2), y + radius * (float) Math.sin(angle2), z);
			Vector3 v3 = new Vector3(x + radius * (float) Math.cos(angle), y + radius * (float) Math.sin(angle), z);
			Vector3 vC1 = v2.minus(v1);
			Vector3 vC2 = v3.minus(v1);
			Vector3 normal = vC2.cross(vC1);
			glNormal3f(normal.x, normal.y, normal.z);
			vertex(v2);
			vertex(v3);
		}
		glEnd();
		
	}
	
	public void drawPolygon(Vector3[] vertices) {
		glBindTexture(GL_TEXTURE_2D, texWhite.getID());
		glBegin(GL_POLYGON);
		for (Vector3 v : vertices) {
			vertex(v);
		}
		glEnd();
	}
	
	public void drawPolygonNormal(Vector3[] vertices, Vector3[] normals) {
		glBindTexture(GL_TEXTURE_2D, texWhite.getID());
		glBegin(GL_POLYGON);
		Vector3 v;
		Vector3 n;
		for (int i = 0; i < vertices.length; i++) {
			v = vertices[i];
			n = normals[i];
			glNormal3f(n.x, n.y, n.z);
			vertex(v);
		}
		glEnd();
	}
	
	public void drawPolygonTextured(Texture tex, Vector3[] vertices, Vector[] textureCoords) {
		glBindTexture(GL_TEXTURE_2D, tex.getID());
		glBegin(GL_POLYGON);
		Vector3 v;
		Vector t;
		for (int i = 0; i < vertices.length; i++) {
			v = vertices[i];
			t = textureCoords[i];
			glTexCoord2f(t.x, 1 - t.y);
			vertex(v);
		}
		glEnd();
	}
	
	public void drawPolygonTexturedNormal(Texture tex, Vector3[] vertices, Vector[] textureCoords, Vector3[] normals) {
		glBindTexture(GL_TEXTURE_2D, tex.getID());
		glBegin(GL_POLYGON);
		Vector3 v;
		Vector t;
		Vector3 n;
		for (int i = 0; i < vertices.length; i++) {
			v = vertices[i];
			t = textureCoords[i];
			n = normals[i];
			glNormal3f(n.x, n.y, n.z);
			glTexCoord2f(t.x, 1 - t.y);
			vertex(v);
		}
		glEnd();
	}

	public void lookAt(float eyeX, float eyeY, float eyeZ, float atX, float atY, float atZ) {
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		gluLookAt(eyeX, eyeY, eyeZ, atX, atY, atZ, 0, 0, 1);
	}

	private void gluPerspective(float fovy, float aspect, float zNear, float zFar) {

		float sine, cotangent, deltaZ;
		float radians = fovy / 2 * (float) Math.PI / 180;

		deltaZ = zFar - zNear;
		sine = (float) Math.sin(radians);

		if ((deltaZ == 0) || (sine == 0) || (aspect == 0)) {
			return;
		}

		cotangent = (float) Math.cos(radians) / sine;

		gluMakeIDentity(matrix);

		matrix.put(0 * 4 + 0, cotangent / aspect);
		matrix.put(1 * 4 + 1, cotangent);
		matrix.put(2 * 4 + 2, -(zFar + zNear) / deltaZ);
		matrix.put(2 * 4 + 3, -1);
		matrix.put(3 * 4 + 2, -2 * zNear * zFar / deltaZ);
		matrix.put(3 * 4 + 3, 0);

		glMultMatrixf(matrix);

	}

	private void gluLookAt(float eyex, float eyey, float eyez, float centerx, float centery, float centerz, float upx, float upy, float upz) {

		float[] forward = this.forward;
		float[] side = this.side;
		float[] up = this.up;

		forward[0] = centerx - eyex;
		forward[1] = centery - eyey;
		forward[2] = centerz - eyez;

		up[0] = upx;
		up[1] = upy;
		up[2] = upz;

		normalize(forward);

		/* Side = forward x up */
		cross(forward, up, side);
		normalize(side);

		/* Recompute up as: up = side x forward */
		cross(side, forward, up);

		gluMakeIDentity(matrix);
		matrix.put(0 * 4 + 0, side[0]);
		matrix.put(1 * 4 + 0, side[1]);
		matrix.put(2 * 4 + 0, side[2]);

		matrix.put(0 * 4 + 1, up[0]);
		matrix.put(1 * 4 + 1, up[1]);
		matrix.put(2 * 4 + 1, up[2]);

		matrix.put(0 * 4 + 2, -forward[0]);
		matrix.put(1 * 4 + 2, -forward[1]);
		matrix.put(2 * 4 + 2, -forward[2]);

		glMultMatrixf(matrix);
		glTranslatef(-eyex, -eyey, -eyez);

	}

	private void gluMakeIDentity(FloatBuffer m) {
		int oldPos = m.position();
		m.put(IDENTITY_MATRIX);
		m.position(oldPos);
	}

	private float[] normalize(float[] v) {
		float r;
		r = (float) Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
		if (r == 0.0) {
			return v;
		}
		r = 1.0f / r;
		v[0] *= r;
		v[1] *= r;
		v[2] *= r;
		return v;
	}

	private void cross(float[] v1, float[] v2, float[] result) {
		result[0] = v1[1] * v2[2] - v1[2] * v2[1];
		result[1] = v1[2] * v2[0] - v1[0] * v2[2];
		result[2] = v1[0] * v2[1] - v1[1] * v2[0];
	}

	public void checkShapeTexture(int shape, int texture) {
		if (currentShape == shape && currentTexture == texture) {
			return;
		}
		glEnd();
		if (currentTexture != texture) {
			currentTexture = texture;
			glBindTexture(GL_TEXTURE_2D, texture);
		}
		glBegin(shape);
		currentShape = shape;
	}

	public void endShape() {
		if (currentShape != 0) {
			glEnd();
			currentShape = 0;
		}
	}

	private void vertex(Vector3 pos) {
		Transform3D t = (Transform3D) transform;
		pos = t.applyTransformations(pos);
		glVertex3f(pos.x, pos.y, pos.z);
	}

}
