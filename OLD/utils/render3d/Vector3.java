package com.adx.eng.utils.render3d;

public class Vector3 {
	
	public float x;
	public float y;
	public float z;
	
	public Vector3(float x, float y, float z) {
		setPosition(x, y, z);
	}
	
	public void setPosition(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vector3 minus(Vector3 v) {
		return new Vector3(x - v.x, y - v.y, z - v.z);
	}
	
	public Vector3 cross(Vector3 v) {
		return new Vector3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
	}
	
	public Vector3 normalize() {
		float lenght = (float) Math.sqrt(x * x + y * y + z * z);
		return new Vector3(x / lenght, y / lenght, z / lenght);
	}
	
}
