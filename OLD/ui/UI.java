package com.adx.eng.ui;

import java.util.ArrayList;

import com.adx.eng.frame.Game;
import com.adx.eng.io.Button;
import com.adx.eng.io.InputManager;
import com.adx.eng.io.Key;
import com.adx.eng.render.Color;
import com.adx.eng.render.GraphicsRenderer;
import com.adx.eng.shapes.CPoint;
import com.adx.eng.shapes.CRectangle;

public class UI {
	
	protected int x;
	protected int y;
	protected int width;
	protected int height;
	private CRectangle r;
	protected boolean enabled = true;
	protected boolean hover = false;
	protected boolean down = false;
	protected boolean click = false;
	protected boolean selected = false;
	protected boolean visible = true;
	private ArrayList<UI> lChildren = new ArrayList<UI>();
	private UI upTab = null;
	private UI tab = null;
	
	public boolean getClick() {
		boolean b = click;
		click = false;
		return b;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		int xDiff = x - this.x;
		for (UI element : lChildren) {
			element.addX(xDiff);
		}
		this.x = x;
		r.x = x;
	}
	
	protected void addX(int xPlus) {
		for (UI element : lChildren) {
			element.addX(xPlus);
		}
		this.x += xPlus;
		r.x += xPlus;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		int yDiff = y - this.y;
		for (UI element : lChildren) {
			element.addY(yDiff);
		}
		this.y = y;
		r.y = y;
	}
	
	protected void addY(int yPlus) {
		for (UI element : lChildren) {
			element.addY(yPlus);
		}
		this.y += yPlus;
		r.y += yPlus;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public ArrayList<UI> getChildren() {
		return lChildren;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public void setTab(UI tab) {
		this.tab = tab;
	}
	
	public void setUpTab(UI upTab) {
		this.upTab = upTab;
	}
	
	public void setTabElements(UI upTab, UI tab) {
		if (this.upTab == null || upTab == null) {
			this.upTab = upTab;
		}
		if (this.tab == null || tab == null) {
			this.tab = tab;
		}
	}

	public void load(UIManager manager, Game game) {
		r = new CRectangle(x, y, width, height);
	}

	public void update(UIManager manager, Game game, InputManager input) {
		
		hover = false;
		down = false;
		if (!enabled) {
			click = false;
			return;
		}
		
		CPoint p = new CPoint(input.getMouseX(), input.getMouseY());
		if (r.collides(p)) {
			hover = true;
			if (input.getButton(Button.LEFT)) {
				down = true;
			} else if (input.getButtonReleased(Button.LEFT)) {
				click = true;
				clicked();
			}
			input.disableMousePosition();
		}
		if (input.getButtonPressed(Button.LEFT)) {
			if (hover) {
				manager.deselectAll();
				selected = true;
				selected();
			} else {
				selected = false;
			}
		}
		if (selected) {
			if (input.getKeyPressed(Key.ENTER)) {
				click = true;
				clicked();
			}
			if (input.getKeyPressed(Key.TAB)) {
				input.releaseKey(Key.TAB);
				if (input.getKey(Key.LEFT_SHIFT) && upTab != null) {
					upTab.setSelected(true);
					upTab.selected();
					selected = false;
				} else if (tab != null) {
					tab.setSelected(true);
					tab.selected();
					selected = false;
				}
			}
		}
		
	}

	public void render(UIManager manager, Game game, GraphicsRenderer g) {
	}
	
	public void delete(UIManager manager) {
	}
	
	protected void clicked() {
	}
	
	protected void selected() {
	}
	
	public void addChildren(UI... elements) {
		for (UI e : elements) {
			lChildren.add(e);
		}
	}
	
	public void setParent(UI element) {
		element.getChildren().add(this);
	}
	
	protected Color addColorOffset(Color base, float f) {
		return new Color(base.r + f, base.g + f, base.b + f, 1.0f);
	}

}
