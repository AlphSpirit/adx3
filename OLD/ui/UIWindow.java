package com.adx.eng.ui;

import com.adx.eng.frame.Game;
import com.adx.eng.io.Button;
import com.adx.eng.io.InputManager;
import com.adx.eng.render.AngelCodeFont;
import com.adx.eng.render.Color;
import com.adx.eng.render.GraphicsRenderer;
import com.adx.eng.shapes.CPoint;
import com.adx.eng.shapes.CRectangle;

public class UIWindow extends UI {
	
	private int mouseXDiff;
	private int mouseYDiff;
	private boolean grabbed = false;
	private CRectangle rGrab;
	private UIButton btnClose;
	private boolean modal;
	private AngelCodeFont font;
	
	public UIWindow(int x, int y, int width, int height, boolean modal, AngelCodeFont font) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		rGrab = new CRectangle(x, y, width - 32, 32);
		this.modal = modal;
		this.font = font;
	}
	
	public void enableCloseButton(boolean enabled) {
		btnClose.visible = enabled;
		btnClose.enabled = enabled;
		rGrab.width = enabled ? width - 32 : width;
	}
	
	public void enableGrabBar(boolean enabled) {
		rGrab.height = enabled ? 32 : 0;
	}
	
	@Override
	public void load(UIManager manager, Game game) {
		
		super.load(manager, game);
		btnClose = new UIButton(x + width - 32, y, 32, 32, "X", font);
		btnClose.setParent(this);
		manager.addElements(btnClose);
		
	}
	
	@Override
	public void update(UIManager manager, Game game, InputManager input) {
		
		super.update(manager, game, input);
		
		if (btnClose.getClick()) {
			manager.removeElement(this);
			return;
		}
		
		if (grabbed) {
			setX(input.getMouseX() - mouseXDiff);
			setY(input.getMouseY() - mouseYDiff);
			if (x < 0) {
				setX(0);
			} else if (x + width > game.getWidth()) {
				setX(game.getWidth() - width);
			}
			if (y < 0) {
				setY(0);
			} else if (y + height > game.getHeight()) {
				setY(game.getHeight() - height);
			}
			if (!input.getButton(Button.LEFT)) {
				grabbed = false;
			}
		}
		if (hover && input.getButtonPressed(Button.LEFT) && rGrab.collides(new CPoint(input.getMouseX(), input.getMouseY()))) {
			mouseXDiff = input.getMouseX() - x;
			mouseYDiff = input.getMouseY() - y;
			grabbed = true;
		}
		if (modal) {
			input.disableMouse();
		}
		
	}
	
	@Override
	public void render(UIManager manager, Game game, GraphicsRenderer g) {
		
		int xDraw = x + game.getViewX();
		int yDraw = y + game.getViewY();
		
		Color base = new Color(0.0f, 0.4f, 0.8f, 1.0f);
		float f = -0.2f;
		Color colBorder = addColorOffset(base, f);
		f = -0.1f;
		Color colGrab = addColorOffset(base, f);
		
		// Draw modal fade
		if (modal) {
			g.setColor(0, 0, 0, 127);
			g.drawRectangle(game.getViewX(), game.getViewY(), game.getWidth(), game.getHeight());
		}
		
		// Draw filling
		g.setColor(base);
		g.drawRectangle(xDraw, yDraw, width, height);
		
		// Draw grab bar
		g.setColor(colGrab);
		g.drawRectangle(rGrab.x + game.getViewX(), rGrab.y + game.getViewY(), rGrab.width, rGrab.height);
		
		// Draw border
		g.setColor(colBorder);
		g.outlineRectangle(xDraw, yDraw, width, height);
		if (rGrab.height > 0) {
			g.drawLine(xDraw, yDraw + 31, xDraw + width, yDraw + 31);
		}
		
	}
	
	@Override
	public void setX(int x) {
		super.setX(x);
		rGrab.x = x;
	}
	
	@Override
	public void setY(int y) {
		super.setY(y);
		rGrab.y = y;
	}

}
