package com.adx.eng.ui;

import com.adx.eng.frame.Game;
import com.adx.eng.io.InputManager;
import com.adx.eng.render.AngelCodeFont;
import com.adx.eng.render.Color;
import com.adx.eng.render.GraphicsRenderer;

public class UIList extends UI {
	
	private AngelCodeFont font;
	private UIListElement[] elements;
	private UIButton btnUp;
	private UIButton btnDown;
	private int selectedElement = -1;
	private int shownElements;
	private int topElement = 0;
	private int grabBarPosition = 32;
	
	public UIList(int x, int y, int width, int height, String[] options, AngelCodeFont font) {
		
		if (height <  128) {
			height = 128;
		}
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.font = font;
		
		shownElements = height / 32;
		elements = new UIListElement[options.length];
		for (int i = 0; i < options.length; i++) {
			elements[i] = new UIListElement(x, y + 32 * i, width - 32, 32, options[i], font);
			if (i >= shownElements) {
				elements[i].setEnabled(false);
				elements[i].setVisible(false);
			}
		}
		
	}
	
	private class UIListElement extends UI {
		
		private boolean selectedElement = false;
		private String text;
		private AngelCodeFont font;
		
		public UIListElement(int x, int y, int width, int height, String text, AngelCodeFont font) {
			
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
			this.text = text;
			this.font = font;
			
		}
		
		public void setSelectedElement(boolean s) {
			selectedElement = s;
		}
		
		@Override
		public void render(UIManager manager, Game game, GraphicsRenderer g) {
			
			if (!visible) {
				return;
			}
			
			Color base = new Color(0.0f, 0.4f, 0.8f, 1.0f);
			float f = -0.2f;
			Color colBorder = addColorOffset(base, f); 
			f = 0.15f;
			Color colSelected = addColorOffset(base, f);
			f = 0.25f;
			Color colSelected2 = addColorOffset(base, f);
			f = 0.1f;
			Color colFill2 = addColorOffset(base, f);
			
			// Draw filling
			if (selectedElement) {
				g.drawRectangleColored(x, y, width, height, colSelected, colSelected, colSelected2, colSelected2);
			} else {
				g.drawRectangleColored(x, y, width, height, colFill2, colFill2, base, base);
			}
			
			// Draw border
			g.setColor(colBorder);
			g.outlineRectangle(x, y, width, height);
			
			// Draw text
			font.setAlign(AngelCodeFont.H_ALIGN_LEFT, AngelCodeFont.V_ALIGN_MIDDLE);
			g.setColor(0, 0, 0, 255);
			font.draw(g, text, x + 2 + 1, y + height / 2 + 1);
			g.setColor(255, 255, 255, 255);
			font.draw(g, text, x + 2, y + height / 2);
			
		}
		
	}
	
	public int getSelectedIndex() {
		return selectedElement;
	}
	
	public void setSelectedIndex(int index) {
		selectedElement = index;
		elements[index].setSelectedElement(true);
		for (int j = 0; j < elements.length; j++) {
			if (index != j) {
				elements[j].setSelectedElement(false);
			}
		}
	}
	
	@Override
	public void load(UIManager manager, Game game) {
		
		super.load(manager, game);
		
		addChildren(elements);
		manager.addElements(elements);
		
		btnUp = new UIButton(x + width - 32, y, 32, 32, "^", font);
		btnDown = new UIButton(x + width - 32, y + height - 32, 32, 32, "v", font);
		addChildren(btnUp, btnDown);
		manager.addElements(btnUp, btnDown);
		
	}
	
	@Override
	public void update(UIManager manager, Game game, InputManager input) {
		
		super.update(manager, game, input);
		
		for (int i = 0; i < elements.length; i++) {
			if (elements[i].getClick()) {
				selectedElement = i;
				elements[i].setSelectedElement(true);
				for (int j = 0; j < elements.length; j++) {
					if (i != j) {
						elements[j].setSelectedElement(false);
					}
				}
			}
		}
		
		if (btnUp.getClick() && topElement > 0) {
			for (int i = 0; i < elements.length; i++) {
				elements[i].addY(32);
			}
			topElement--;
			int show = topElement;
			if (show >= 0) {
				elements[show].setEnabled(true);
				elements[show].setVisible(true);
			}
			int hide = topElement + shownElements;
			if (hide < elements.length) {
				elements[hide].setEnabled(false);
				elements[hide].setVisible(false);
			}
		}
		
		if (btnDown.getClick() && topElement < elements.length - shownElements) {
			for (int i = 0; i < elements.length; i++) {
				elements[i].addY(-32);
			}
			topElement++;
			int show = topElement + shownElements - 1;
			if (show < elements.length) {
				elements[show].setEnabled(true);
				elements[show].setVisible(true);
			}
			int hide = topElement - 1;
			if (hide >= 0) {
				elements[hide].setEnabled(false);
				elements[hide].setVisible(false);
			}
		}
		
		grabBarPosition = (int) ((topElement / (float) (elements.length - shownElements)) * (height - 128)) + 32;
		
	}
	
	@Override
	public void render(UIManager manager, Game game, GraphicsRenderer g) {
		
		Color base = new Color(0.0f, 0.4f, 0.8f, 1.0f);
		float f = -0.4f;
		Color colGrabBorder = addColorOffset(base, f);
		f = -0.2f;
		Color colGrabBorder2 = addColorOffset(base, f);
		f = 0.1f;
		Color colGrabFill2 = addColorOffset(base, f);
		f = -0.1f;
		Color colFill = addColorOffset(base, f);
		
		g.setColor(colFill);
		g.drawRectangle(x, y, width, height);
		
		// Draw grab bar
		g.drawRectangleColored(x + width - 32, y + grabBarPosition, 32, 64, base, base, colGrabFill2, colGrabFill2);
		g.setColor(colGrabBorder);
		g.outlineRectangle(x + width - 32, y + grabBarPosition, 32, 64);
		g.setColor(colGrabBorder2);
		g.outlineRectangle(x + width - 32 + 1, y + grabBarPosition + 1, 30, 62);
		g.setColor(colGrabBorder2);
		g.drawLine(x + width - 32 + 8, y + grabBarPosition + 32 - 4, x + width - 32 + 24, y + grabBarPosition + 32 - 4);
		g.drawLine(x + width - 32 + 8, y + grabBarPosition + 32, x + width - 32 + 24, y + grabBarPosition + 32);
		g.drawLine(x + width - 32 + 8, y + grabBarPosition + 32 + 4, x + width - 32 + 24, y + grabBarPosition + 32 + 4);
		
	}

}
