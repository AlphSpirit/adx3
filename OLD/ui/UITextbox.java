package com.adx.eng.ui;

import com.adx.eng.frame.Game;
import com.adx.eng.io.InputManager;
import com.adx.eng.render.AngelCodeFont;
import com.adx.eng.render.Color;
import com.adx.eng.render.GraphicsRenderer;

public class UITextbox extends UI {
	
	private int maxLength;
	private AngelCodeFont font;
	private String text = "";
	private boolean password;
	private int blinkTimer = 0;
	private int hoverTimer = 0;
	private int downTimer = 0;
	private int clickTimer = 0;
	private float selectTimer = 0;
	private InputManager input;
	
	public UITextbox(int x, int y, int width, int height, int maxLength, boolean password, AngelCodeFont font) {
		
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.maxLength = maxLength;
		this.password = password;
		this.font = font;
		
	}
	
	public String getText() {
		return text;
	}
	
	@Override
	public void load(UIManager manager, Game game) {
		super.load(manager, game);
		input = game.getInput();
	}
	
	@Override
	public void update(UIManager manager, Game game, InputManager input) {
		
		super.update(manager, game, input);
		
		if (hover && hoverTimer < 10) {
			hoverTimer++;
		} else if (!hover && hoverTimer > 0) {
			hoverTimer--;
		}
		
		if (down && downTimer < 5) {
			downTimer++;
		} else if (!down && downTimer > 0) {
			downTimer--;
		}
		
		if (clickTimer > 0) {
			clickTimer--;
		}
		
		if (selected) {
			text = input.getKeyboardString();
			blinkTimer++;
		}
		
		selectTimer += 0.1f;
		
	}
	
	@Override
	public void render(UIManager manager, Game game, GraphicsRenderer g) {
		
		int xDraw = x + game.getViewX();
		int yDraw = y + game.getViewY();
		
		Color base = new Color(0.0f, 0.3f, 0.6f, 1.0f);
		float f = -0.4f + hoverTimer / 10.0f + clickTimer / 30.0f;
		if (selected) {
			f += 1;
		}
		Color colUnderline = addColorOffset(base, f);
		f = hoverTimer / 30.0f + clickTimer / 30.0f;
		if (selected) {
			f += 0.2f + (float) (Math.cos(selectTimer) + 1) / 8.0f;
		}
		Color colBorder = addColorOffset(base, f);
		f = -0.1f - clickTimer / 480.0f;
		Color colFill1 = addColorOffset(base, f);
		f = 0 - clickTimer / 480.0f;
		Color colFill2 = addColorOffset(base, f);
		
		// Draw outline
		g.setColor(colBorder);
		g.drawRectangle(xDraw, yDraw, width, height);
		
		// Draw fill
		g.drawRectangleColored(xDraw + 1, yDraw + 1, width - 2, height - 3, colFill1, colFill1, colFill2, colFill2);
		
		// Draw underline
		g.setColor(colUnderline);
		g.drawLine(xDraw, yDraw + height, xDraw + width, yDraw + height);
		
		// Draw text
		font.setAlign(AngelCodeFont.H_ALIGN_LEFT, AngelCodeFont.V_ALIGN_MIDDLE);
		String str = "";
		if (selected) {
			str = input.getKeyboardString();
		} else {
			str = text;
		}
		if (password) {
			str = str.replaceAll(".", "*");
		}
		g.setColor(0, 0, 0, 255);
		font.draw(g, str, xDraw + 3, yDraw + height / 2);
		g.setColor(255, 255, 255, 255);
		font.draw(g, str, xDraw + 2, yDraw + height / 2 - 1);
		if (selected) {
			if (blinkTimer % 60 < 30) {
				String sub = str.substring(0, input.getKeyboardStringIndex());
				int w = font.getWidth(sub);
				g.setColor(0, 0, 0, 255);
				font.draw(g, "|", xDraw + w + 1, yDraw + height / 2);
				g.setColor(255, 255, 255, 255);
				font.draw(g, "|", xDraw + w, yDraw + height / 2 - 1);
			}
		}
		
	}
	
	@Override
	protected void clicked() {
		clickTimer = 30;
	}
	
	@Override
	protected void selected() {
		input.setKeyboardString(text);
		input.setKeyboardStringLimit(maxLength);
		blinkTimer = 0;
	}

}
