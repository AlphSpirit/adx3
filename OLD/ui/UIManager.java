package com.adx.eng.ui;

import java.util.ArrayList;

import com.adx.eng.frame.Game;
import com.adx.eng.frame.Instance;
import com.adx.eng.io.InputManager;
import com.adx.eng.render.GraphicsRenderer;

public class UIManager extends Instance {
	
	private Game game;
	private ArrayList<UI> lUI = new ArrayList<UI>();
	
	@Override
	public void load(Game game) {
		this.game = game;
	}
	
	public void addElements(UI... elements) {
		for (UI e : elements) {
			lUI.add(e);
			e.load(this, game);
		}
	}
	
	public void removeElement(UI element) {
		for (UI e : element.getChildren()) {
			e.delete(this);
			lUI.remove(e);
		}
		element.delete(this);
		lUI.remove(element);
	}
	
	public void setTabChain(UI... elements) {
		UI previous;
		UI next;
		for (int i = 0; i < elements.length; i++) {
			elements[i].setTabElements(null, null);
		}
		for (int i = 0; i < elements.length; i++) {
			if (i == 0) {
				previous = elements[elements.length - 1];
			} else {
				previous = elements[i - 1];
			}
			if (i == elements.length - 1) {
				next = elements[0];
			} else {
				next = elements[i + 1];
			}
			elements[i].setTabElements(previous, next);
		}
	}

	public void deselectAll() {
		for (UI element : lUI) {
			element.setSelected(false);
		}
	}

	@Override
	public void update(Game game, InputManager input) {
		
		UI element;
		for (int i = lUI.size() - 1; i >= 0; i--) {
			element = lUI.get(i);
			element.update(this, game, input);
		}
		
	}

	@Override
	public void render(Game game, GraphicsRenderer g) {
		
		for (UI element : lUI) {
			element.render(this, game, g);
		}
		
	}

}
