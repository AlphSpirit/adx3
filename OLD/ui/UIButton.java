package com.adx.eng.ui;

import com.adx.eng.frame.Game;
import com.adx.eng.io.InputManager;
import com.adx.eng.render.AngelCodeFont;
import com.adx.eng.render.Color;
import com.adx.eng.render.GraphicsRenderer;

public class UIButton extends UI {
	
	private String text;
	private AngelCodeFont font;
	private int hoverTimer = 0;
	private int downTimer = 0;
	private int clickTimer = 0;
	private float selectTimer = 0;
	
	public UIButton(int x, int y, int width, int height, String text, AngelCodeFont font) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.text = text;
		this.font = font;
	}
	
	@Override
	public void update(UIManager manager, Game game, InputManager input) {
		
		super.update(manager, game, input);
		
		if (hover && hoverTimer < 10) {
			hoverTimer++;
		} else if (!hover && hoverTimer > 0) {
			hoverTimer--;
		}
		
		if (down && downTimer < 5) {
			downTimer++;
		} else if (!down && downTimer > 0) {
			downTimer--;
		}
		
		if (clickTimer > 0) {
			clickTimer--;
		}
		
		selectTimer += 0.1f;
		
	}

	@Override
	public void render(UIManager manager, Game game, GraphicsRenderer g) {
		
		if (!visible) {
			return;
		}
		
		int xDraw = x + game.getViewX();
		int yDraw = y + game.getViewY();
		
		// Create colors
		Color base = new Color(0.0f, 0.3f, 0.6f, 1.0f);
		float f = -0.2f;
		if (selected) {
			f += 0.6f + (float) (Math.cos(selectTimer)) / 8.0f;
		}
		Color colBorder = addColorOffset(base, f);
		f = 0.2f + hoverTimer / 120.0f + downTimer / 60.0f + clickTimer / 240.0f;
		Color colFill1 = addColorOffset(base, f);
		f -= 0.2f;
		Color colFill2 = addColorOffset(base, f);
		
		// Draw border
		g.setColor(colBorder);
		g.drawRectangle(xDraw, yDraw, width, height);
		
		// Draw filling
		g.drawRectangleColored(xDraw + 1, yDraw + 1, width - 2, height - 2, colFill1, colFill1, colFill2, colFill2);
		
		// Draw text
		int textOffset = down ? 1 : 0;
		font.setAlign(AngelCodeFont.H_ALIGN_CENTER, AngelCodeFont.V_ALIGN_MIDDLE);
		g.setColor(0, 0, 0, 255);
		font.draw(g, text, xDraw + width / 2 + 1, yDraw + height / 2 + textOffset + 1);
		g.setColor(255, 255, 255, 255);
		font.draw(g, text, xDraw + width / 2, yDraw + height / 2 + textOffset);
		
	}
	
	@Override
	protected void clicked() {
		clickTimer = 30;
	}

}
