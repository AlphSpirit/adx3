package com.adx.eng.ui;

import com.adx.eng.frame.Game;
import com.adx.eng.render.AngelCodeFont;
import com.adx.eng.render.GraphicsRenderer;

public class UILabel extends UI {
	
	private String text;
	private AngelCodeFont font;
	
	public UILabel(int x, int y, String text, AngelCodeFont font) {
		
		this.x = x;
		this.y = y;
		this.width = font.getWidth(text) + 1;
		this.height = font.getHeight(text);
		this.text = text;
		this.font = font;
		
	}
	
	@Override
	public void render(UIManager manager, Game game, GraphicsRenderer g) {
		
		int xDraw = x + game.getViewX();
		int yDraw = y + game.getViewY();
		
		font.setAlign(AngelCodeFont.H_ALIGN_LEFT, AngelCodeFont.V_ALIGN_MIDDLE);
		g.setColor(0, 0, 0, 255);
		font.draw(g, text, xDraw + 1, yDraw + height / 2 + 1);
		g.setColor(255, 255, 255, 255);
		font.draw(g, text, xDraw, yDraw + height / 2);
		
	}

}
