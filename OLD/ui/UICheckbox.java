package com.adx.eng.ui;

import com.adx.eng.frame.Game;
import com.adx.eng.io.InputManager;
import com.adx.eng.render.AngelCodeFont;
import com.adx.eng.render.Color;
import com.adx.eng.render.GraphicsRenderer;

public class UICheckbox extends UI {
	
	private String text;
	private AngelCodeFont font;
	private int hoverTimer = 0;
	private int downTimer = 0;
	private int clickTimer = 0;
	private boolean checked = false;
	private float selectTimer = 0;
	
	public UICheckbox(int x, int y, int size, String text, AngelCodeFont font) {
		this.x = x;
		this.y = y;
		this.width = size;
		this.height = size;
		this.text = text;
		this.font = font;
	}
	
	public boolean getChecked() {
		return checked;
	}
	
	public void setChecked(boolean c) {
		checked = c;
	}
	
	@Override
	public void update(UIManager manager, Game game, InputManager input) {
		
		super.update(manager, game, input);
		
		if (hover && hoverTimer < 10) {
			hoverTimer++;
		} else if (!hover && hoverTimer > 0) {
			hoverTimer--;
		}
		
		if (down && downTimer < 5) {
			downTimer++;
		} else if (!down && downTimer > 0) {
			downTimer--;
		}
		
		if (click) {
			clickTimer = 30;
		} else if (clickTimer > 0) {
			clickTimer--;
		}
		
		selectTimer += 0.1f;
		
	}
	
	@Override
	public void render(UIManager manager, Game game, GraphicsRenderer g) {
		
		int xDraw = x + game.getViewX();
		int yDraw = y + game.getViewY();
		
		// Create colors
		Color base = new Color(0.0f, 0.3f, 0.6f, 1.0f);
		float f = -0.2f;
		if (selected) {
			f += 0.6 + (float) (Math.cos(selectTimer)) / 8.0f;
		}
		Color colBorder = addColorOffset(base, f);
		f = 0.4f;
		Color colBorder2 = addColorOffset(base, f);
		f = 0.2f + hoverTimer / 120.0f + downTimer / 60.0f + clickTimer / 240.0f;
		Color colFill1 = addColorOffset(base, f);
		f -= 0.2f;
		Color colFill2 = addColorOffset(base, f);
		
		// Draw borders
		g.setColor(colBorder);
		g.drawRectangle(xDraw, yDraw, width, height);
		g.setColor(colBorder2);
		g.drawRectangle(xDraw + 1, yDraw + 1, width - 2, height - 2);
		
		// Draw filling
		g.drawRectangleColored(xDraw + 2, yDraw + 2, width - 4, height - 4, colFill1, colFill1, colFill2, colFill2);
		
		// Draw text
		font.setAlign(AngelCodeFont.H_ALIGN_LEFT, AngelCodeFont.V_ALIGN_MIDDLE);
		g.setColor(0, 0, 0, 255);
		font.draw(g, text, xDraw + height + 4 + 1, yDraw + height / 2 + 1);
		g.setColor(255, 255, 255, 255);
		font.draw(g, text, xDraw + height + 4, yDraw + height / 2);
		
		// Draw click
		if (checked) {
			g.setColor(255, 255, 255, 255);
			g.outlineRectangle(xDraw + 4, yDraw + 4, width - 8, height - 8);
		}
		
	}
	
	@Override
	protected void clicked() {
		checked = !checked;
	}

}
