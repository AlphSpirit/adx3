package com.adx.eng.ui;

import com.adx.eng.frame.Game;
import com.adx.eng.io.InputManager;
import com.adx.eng.render.AngelCodeFont;
import com.adx.eng.render.GraphicsRenderer;

public class UIRadioSet extends UI {
	
	private int num;
	private String[] options;
	private UICheckbox[] boxes;
	private int selected = 0;
	private int checkWidth;
	private int checkHeight;
	private AngelCodeFont font;
	
	public UIRadioSet(int x, int y, int width, int height, String[] options, AngelCodeFont font) {
		this.x = x;
		this.y = y;
		this.options = options;
		num = options.length;
		this.width = 0;
		this.height = 0;
		checkWidth = height;
		checkHeight = height;
		this.font = font;
	}
	
	public int getSelected() {
		return selected;
	}
	
	public String getSelectedText() {
		return options[selected];
	}
	
	public UICheckbox[] getCheckboxes() {
		return boxes;
	}
	
	@Override
	public void setTabElements(UI upTab, UI tab) {
		if (upTab == null || tab == null) {
			return;
		}
		UI previous;
		UI next;
		for (int i = 0; i < num; i++) {
			if (i == 0) {
				previous = upTab;
			} else {
				previous = boxes[i - 1];
			}
			if (i == num - 1) {
				next = tab;
			} else {
				next = boxes[i + 1];
			}
			boxes[i].setTabElements(previous, next);
		}
		upTab.setTab(boxes[0]);
		tab.setUpTab(boxes[boxes.length - 1]);
	}
	
	@Override
	public void load(UIManager manager, Game game) {
		super.load(manager, game);
		boxes = new UICheckbox[num];
		for (int i = 0; i < num; i++) {
			boxes[i] = new UICheckbox(x, y + i * (checkHeight + 8), checkHeight, options[i], font);
		}
		manager.addElements(boxes);
		boxes[0].setChecked(true);
	}
	
	@Override
	public void update(UIManager manager, Game game, InputManager input) {
		
		super.update(manager, game, input);
		
		for (int i = 0; i < num; i++) {
			if (boxes[i].getClick()) {
				boxes[i].setChecked(true);
				selected = i;
				for (int j = 0; j < num; j++) {
					if (j != i) {
						boxes[j].setChecked(false);
					}
				}
			}
		}
		
	}
	
	@Override
	public void render(UIManager manager, Game game, GraphicsRenderer g) {
		
		int xDraw = x + game.getViewX();
		int yDraw = y + game.getViewY();
		
		g.setColor(0, 0, 0, 255);
		for (int i = 0; i < num - 1; i++) {
			g.drawLine(xDraw + checkWidth / 2, yDraw + i * (checkHeight + 8) + checkHeight, xDraw + checkWidth / 2, yDraw + (i + 1) * (checkHeight + 8));
		}
		
	}
	
	@Override
	public void delete(UIManager manager) {
		super.delete(manager);
		for (int i = 0; i < num; i++) {
			manager.removeElement(boxes[i]);
		}
	}
	
	@Override
	public void setX(int x) {
		super.setX(x);
		for (int i = 0; i < num; i++) {
			boxes[i].setX(x);
		}
	}
	
	@Override
	public void setY(int y) {
		super.setY(y);
		for (int i = 0; i < num; i++) {
			boxes[i].setY(y);
		}
	}
	
	@Override
	protected void addX(int xPlus) {
		super.addX(xPlus);
		for (int i = 0; i < num; i++) {
			boxes[i].addX(xPlus);
		}
	}
	
	@Override
	protected void addY(int yPlus) {
		super.addY(yPlus);
		for (int i = 0; i < num; i++) {
			boxes[i].addY(yPlus);
		}
	}

}
